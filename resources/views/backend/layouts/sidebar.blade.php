 @php
    $prefix   = Request::route()->getPrefix();
    $route    = Route::current()->getName();
 @endphp
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('public/backend/img/AdminLTELogo.png')}}" alt="Admin Dashboard" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin DASHBOARD</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('public/backend/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="{{route('dashboard')}}" class="nav-link {{$route == 'dashboard'?'active':''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>
         
          <li class="nav-item has-treeview {{$prefix == '/site'?'menu-open':''}}">
            <a href="#" class="nav-link {{$prefix == '/site'?'active':''}}">
              <i class="nav-icon fas fa-tools"></i>
              <p>
                Settings
                <i class="fas fa-angle-left right"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview" style="{{$prefix == '/site'?'display:block':'display:none'}}">
              <li class="nav-item">
                <a href="{{route('view.introduction')}}" class="nav-link {{$route == 'view.intorduction'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View Introduction</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{route('site.setting')}}" class="nav-link {{$route == 'site.setting'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Edit Site Logo</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('police.officer.show')}}" class="nav-link {{$route == 'police.officer.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Police Officers</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{route('download.document.show')}}" class="nav-link {{$route == 'download.document.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Document Download</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{route('photo.gallery.show')}}" class="nav-link {{$route == 'photo.gallery.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Photo Gallery</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('criminal.list.show')}}" class="nav-link {{$route == 'criminal.list.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Criminal List</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('police.staff.show')}}" class="nav-link {{$route == 'police.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Police Staff</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{route('mission.vision.show')}}" class="nav-link {{$route == 'mission.vision.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Mission And Vission</p>
                </a>
              </li>    
              <li class="nav-item">
                <a href="{{route('view.site.news')}}" class="nav-link {{$route == 'view.site.news'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View News</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="{{route('site.police.statement.show')}}" class="nav-link {{$route == 'site.police.statement.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Police Statement</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('information.provider.show')}}" class="nav-link {{$route == 'information.provider.show'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inrformation Provider</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('site.necessary.link')}}" class="nav-link {{$route == 'site.necessary.link'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Necessary links</p>
                </a>
              </li>            
              <li class="nav-item">
                <a href="{{route('site.slider')}}" class="nav-link {{$route == 'site.slider'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View Slider</p>
                </a>
              </li>                                    
            </ul>
          </li>


          <li class="nav-item has-treeview {{$prefix == '/site'?'menu-open':''}}">
            <a href="#" class="nav-link {{$prefix == '/site'?'active':''}}">
              <i class="nav-icon fas fa-tools"></i>
              <p>
                Notice Board
                <i class="fas fa-angle-left right"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview" style="{{$prefix == '/site'?'display:block':'display:none'}}">
              
              <li class="nav-item">
                <a href="{{route('site.notice')}}" class="nav-link {{$route == 'site.notice'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Notice</p>
                </a>
              </li>  
              <li class="nav-item">
                <a href="{{route('site.noc')}}" class="nav-link {{$route == 'site.noc'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>NOC</p>
                </a>
              </li>             
                                               
            </ul>
          </li>



          <li class="nav-item has-treeview {{$prefix == '/tour-package'?'menu-open':''}}">
            <a href="#" class="nav-link {{$prefix == '/tour-package'?'active':''}}">
              <i class="nav-icon fas fa-plane"></i>
              <p>
                Travel Package
                <i class="fas fa-angle-left right"></i>                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('tour-package.view')}}" class="nav-link {{$route == 'tour-package.view'?'active':''}}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View Package</p>
                </a>
              </li>                                  
            </ul>
          </li>

         {{--  <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Sample Dropdown
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sub Menu 1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Sub Menu 2</p>
                </a>
              </li>                         
            </ul>
          </li>  --}}         
          
          <li class="nav-header">LABELS</li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Important</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-warning"></i>
              <p>Warning</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>Informational</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>