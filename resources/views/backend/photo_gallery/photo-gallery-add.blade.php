@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ !empty($editData)?'Edit Information Provider':'Add Informatino Provider' }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Photo Gallery</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('photo.gallery.show')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Police Officer</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('photo.gallery.update',$editData->id) : route('photo.gallery.store') }}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                
                                
                                <div class="form-group col-sm-12">
                                  <label>Photo</label>
                                  <input  value="{{!empty($editData)? $editData->image:''}}" class="form-control" type="file" name="image"><span>@if($errors){{ $errors->first('image')}} @endif</span>
                                </div>  

                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

