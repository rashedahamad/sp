@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Photos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Photo Gallery</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card"> 
	        		<div class="card-header">

                <a href="{{route('photo.gallery.add')}}" class="btn btn-sm btn-info"><i class="fas fa-plus"></i>Add photo</a>
              
	        			
	        		</div>
		            <div class="card-body">

		              <table id="dataTable" class="table table-sm table-bordered table-striped">
		                <thead>
		                <tr>
		                  <th>SL</th>
		                  <th>Photo</th>
		                  
                      <th>Action</th>
		                </tr>
		                </thead>
		                <tbody>
                     
                      @foreach($photo as $value)
                      <tr>
                        <td width="80">{{ $loop->iteration }}</td>
                        <td><center><img src="{{asset('public/uploads/photoGallery/'.$value->image)}}" alt="NO Image" height="50"></center>
                       </td>
                        
                        <td width="100"><a href="{{ route('photo.gallery.edit',$value->id) }}" class="btn btn-primary btn-flat btn-sm edit"><i class="fas fa-edit"></i></a> | <a href="{{ route('photo.gallery.delete',$value->id) }}" class="btn btn-danger btn-flat btn-sm delete"><i class="fas fa-trash"></i></a></td>
                      </tr>
                      @endforeach
                                 
		                </tbody>                
		              </table>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

