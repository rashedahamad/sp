@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Link</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Links</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('site.necessary.link')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Links</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('site.necessary.link.update',$editData->id): route('site.necessary.link.store')}}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label>Link Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->link_name:'' }}" class="form-control" name="link_name" ><span>@if($errors){{$errors->first('link_name') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Link Address</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->link_address:'' }}" class="form-control" name="link_address"></input><span>@if($errors){{$errors->first('link_address') }}@endif</span>
                                </div> 
                                 
                        
	                                                   	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

