@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Download</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Download</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('site.necessary.link')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Downloads</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('download.document.update',$editData->id): route('download.document.store')}}" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label>Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->name:'' }}" class="form-control" name="name" ><span>@if($errors){{$errors->first('name') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Publish Date</label>
                                  <input type="date" value="{{ !empty($editData)? $editData->publish_date:'' }}" class="form-control" name="publish_date">
                                  <span>@if($errors){{$errors->first('publish_date') }}@endif</span>
                                </div> 

                                <div class="form-group col-sm-6">
                                  <label></label>
                                  <input type="file" class="form-control" name="file">
                                  <span>@if($errors){{$errors->first('file') }}@endif</span>
                                </div>
                          </div>

                                 
                        
	                                                   	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

