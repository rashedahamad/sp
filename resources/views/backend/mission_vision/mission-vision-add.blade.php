@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ !empty($editData)? 'Edit Mission And Vision':'Add Mission And Vision' }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Mission And Vision</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('mission.vision.show')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Mission and Vision</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('mission.vision.update',$editData->id): route('mission.vision.store')}}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-12">
                                  <label>Mission</label>
                                  
                                  <textarea type="textArea" value="{{ !empty($editData)? $editData->mission:'' }}" class="form-control" name="mission">{{ !empty($editData)? $editData->mission:'' }}</textarea><span>@if($errors){{$errors->first('mission') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-12">
                                  <label>Vision</label>
                                  <textarea type="textArea" value="{{ !empty($editData)? $editData->vision:'' }}" class="form-control" name="vision">{{ !empty($editData)? $editData->vision:'' }}</textarea><span>@if($errors){{$errors->first('vision') }}@endif</span>
                                </div> 
                                 
                        
	                                                   	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

