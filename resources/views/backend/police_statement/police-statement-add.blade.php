@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ !empty($editData)?'Edit Police Statement':'Add Police Statement' }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Superintendent of Police Statement</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('site.police.statement.show')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Police Statement</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('site.police.statement.update',$editData->id) : route('site.police.statement.store') }}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label>Police Officer Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->police_name : ''  }}" class="form-control" name="police_name" ><span>@if($errors){{$errors->first('police_name') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Police Officer Designation</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->police_designation : ''  }}" class="form-control" name="police_designation" ><span>@if($errors){{$errors->first('police_designation') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Joining Date</label>
                                  <input type="date" value="{{ !empty($editData)? $editData->police_joining_date : ''  }}" class="form-control" name="police_joining_date" ><span>@if($errors){{$errors->first('police_joining_date') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>End Date</label>
                                  <input type="date" value="{{ !empty($editData)? $editData->police_end_date : ''  }}" class="form-control" name="police_end_date" ><span>@if($errors){{$errors->first('police_end_date') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Statement Title</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->police_statement_title : ''  }}" class="form-control" name="police_statement_title" ><span>@if($errors){{$errors->first('police_statement_title') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Superintendent of Police Image</label>
                                  <input  value="{{!empty($editData)? $editData->police_image:''}}" class="form-control" type="file" name="police_image"><span>@if($errors){{$errors->first('police_image') }}@endif</span>
                                </div>  
                                <div class="form-group col-sm-6">
                                  <label>Serial</label>
                                  <input  value="{{!empty($editData)? $editData->serial:''}}" class="form-control" type="number" name="serial"><span>@if($errors){{$errors->first('serial') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-12">
                                  <label>Statement Short Description</label>
                                  <textarea type="textArea" value="{{!empty($editData)? $editData->police_short_description:''}}" class="form-control" name="police_short_description">{{!empty($editData)? $editData->police_short_description:''}}</textarea><span>@if($errors){{$errors->first('police_short_description') }}@endif</span>                             
                            </div>
                             <div class="form-group col-sm-12">
                                  <label>Statement Long Description</label>
                                  <textarea type="textArea" value="{{!empty($editData)? $editData->police_long_description:''}}" class="form-control" name="police_long_description">{{!empty($editData)? $editData->police_long_description:''}}</textarea><span>@if($errors){{$errors->first('police_long_description') }}@endif</span>                             
                            </div>
                            
                                                  	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

