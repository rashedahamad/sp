@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add News</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">News</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('site.noc')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View News Titele</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('site.noc.update',$editData->id) : route('site.noc.store') }}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label>Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->noc_name:'' }}" class="form-control" name="noc_name" ><span>@if($errors){{$errors->first('noc_name') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Father Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->noc_father_name:'' }}" class="form-control" name="noc_father_name"></input><span>@if($errors){{$errors->first('noc_father_name') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Address</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->noc_address:'' }}" class="form-control" name="noc_address"></input><span>@if($errors){{$errors->first('noc_address') }}@endif</span>
                                </div> 
                        
	                              <div class="form-group col-sm-6">
	                                <label>News Image</label>
	                                <input type="file" class="form-control" name="noc_image" accept="image/*"><span>@if($errors){{$errors->first('noc_image') }}@endif</span>
	                              </div>                               
                            </div>                         	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

