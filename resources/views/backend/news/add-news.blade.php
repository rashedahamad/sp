@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add News</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">News</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('view.site.news')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View News Titele</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('update.site.news',$editData->id) : route('store.site.news') }}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-12">
                                  <label>News Tile</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->news_title : '' }}" class="form-control" name="news_title" ><span>@if($errors){{ $errors->first('news_title')}}@endif</span>
                                </div> 
                                <div class="form-group col-sm-12">
                                  <label>Short News</label>
                                  <textarea type="textArea" value="{{ !empty($editData)? $editData->news_short : '' }}" class="form-control" name="news_short">{{ !empty($editData)? $editData->news_short : '' }}</textarea><span>@if($errors){{ $errors->first('news_short')}}@endif</span>

                                </div> 
                                <div class="form-group col-sm-12">
                                  <label>Described News</label>
                                  <textarea type="textArea" value="{{ !empty($editData)? $editData->news_large : '' }}" class="form-control" name="news_large">{{ !empty($editData)? $editData->news_large : '' }}</textarea><span>@if($errors){{ $errors->first('news_large')}}@endif</span>
                                </div> 
                                <div class="form-group col-sm-12">
                                  <label>News Date</label>
                                  <input type="Date" value="{{ !empty($editData)? $editData->news_date : '' }}"  class="form-control" name="news_date" ><span>@if($errors){{ $errors->first('news_date')}}@endif</span>
                                </div>
	                              <div class="form-group col-sm-12">
	                                <label>News Image</label>
	                                <input type="file" class="form-control" name="news_image" accept="image/*"><span>@if($errors){{ $errors->first('news_image')}}@endif</span>
	                              </div>  
                                                             
                            </div>                         	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    Save</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

