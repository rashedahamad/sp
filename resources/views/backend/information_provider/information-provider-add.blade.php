@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ !empty($editData)?'Edit Information Provider':'Add Informatino Provider' }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Information Providing Officer</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('information.provider.show')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Police Statement</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('information.provider.update',$editData->id) : route('information.provider.store') }}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label>Information Providing Officer Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->information_provider_name : ''  }}" class="form-control" name="information_provider_name" ><span>@if($errors){{$errors->first('information_provider_name') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Information Providing Officer Designation</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->information_provider_designation : ''  }}" class="form-control" name="information_provider_designation" ><span>@if($errors){{$errors->first('information_provider_designation') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Information Providing Officer Address</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->information_provider_address : ''  }}" class="form-control" name="information_provider_address" ><span>@if($errors){{$errors->first('information_provider_address') }}@endif</span>
                                </div> 
                                
                                <div class="form-group col-sm-6">
                                  <label>Information Provider Officer Image</label>
                                  <input  value="{{!empty($editData)? $editData->police_image:''}}" class="form-control" type="file" name="information_provider_image"><span>@if($errors){{$errors->first('information_provider_image') }}@endif</span>
                                </div>  
                                
                             
                            
                                                  	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

