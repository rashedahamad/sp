@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ !empty($editData)?'Edit Criminal List':'Add Criminal List' }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Criminal List</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('criminal.list.show')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Criminal List</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ !empty($editData)? route('criminal.list.update',$editData->id) : route('criminal.list.store') }}" enctype="multipart/form-data">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-6">
                                  <label>Criminal Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->criminal_name : ''  }}" class="form-control" name="criminal_name" ><span>@if($errors){{ $errors->first('criminal_name')}} @endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Criminal Father's Name</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->criminal_father : ''  }}" class="form-control" name="criminal_father" ><span>@if($errors){{ $errors->first('criminal_father')}} @endif</span>
                                </div> 
                                <div class="form-group col-sm-6">
                                  <label>Criminal Address</label>
                                  <input type="text" value="{{ !empty($editData)? $editData->criminal_address : ''  }}" class="form-control" name="criminal_address" ><span>@if($errors){{ $errors->first('criminal_address')}} @endif</span>
                                </div>
                                
                                <div class="form-group col-sm-6">
                                  <label>Criminal Image</label>
                                  <input type="file" value="{{!empty($editData)? $editData->criminal_image:''}}" class="form-control"  name="criminal_image"><span>@if($errors){{ $errors->first('criminal_image')}} @endif</span>
                                </div>  
                                <div class="form-group col-sm-12">
                                  <label>Criminal Description</label>
                                  <textarea type="textArea" value="{{ !empty($editData)? $editData->news_short : '' }}" class="form-control" name="criminal_description">{{ !empty($editData)? $editData->criminal_description : '' }}</textarea><span>@if($errors){{ $errors->first('criminal_description')}}@endif</span>

                                </div> 
                                
                             
                            
                                                  	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    {{ !empty($editData)? 'Update':'Save' }}</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

