@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Introduction</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Introduction</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card">
	        		<div class="card-header">
	        			<a href="{{route('view.introduction')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i>View Introduction Titele</a>
	        		</div>
		            <div class="card-body">
		           		<form method="post" action="{{ route('update.introduction',$editData->id) }}">
                    @csrf
                    		 <div class="form-row">
                                <div class="form-group col-sm-12">
                                  <label>Introduction Title</label>
                                  <input type="text" value="{{ $editData->introduction_title }}" class="form-control" name="introduction_title" ><span>@if($errors){{$errors->first('introduction_title') }}@endif</span>
                                </div> 
                                <div class="form-group col-sm-12">
                                  <label>Introduction Description</label>
                                  <textarea type="textArea" value="{{$editData->introduction_description}}" class="form-control" name="introduction_description">{{$editData->introduction_description}}</textarea><span>@if($errors){{$errors->first('introduction_description') }}@endif</span>                             
                            </div>                         	 
                   
                    		<button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> 
		                    Update</button>
		                </form>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

