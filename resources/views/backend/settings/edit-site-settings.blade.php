@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Site Settings</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Site Settings </li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">

        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title">
              <i class="fas fa-edit"></i>
             Edit Settings
            </h3>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('add.site.logo',$data->id) }}" enctype="multipart/form-data">
                  @csrf
                    <ul class="nav nav-tabs" id="custom-content-below-tab" role="tablist">
                              
                              <div class="form-group col-sm-6">
                                <label>Site Image</label><br>
                                <img src="{{ asset('public/uploads/logo/site_logo/'.$data->site_logo)}}" style="width: 100px;height: 100px">
                              </div>
                              <div class="form-group col-sm-6">
                                <label>Government Image</label><br>
                                <img src="{{ asset('public/uploads/logo/govt_logo/'.$data->govt_logo) }}" style="width: 100px;height: 100px">
                              </div>
                              <div class="form-group col-sm-6">
                                <label>Site Logo</label>
                                <input type="file" class="form-control" name="site_logo" accept="image/*">
                                <span class="badge badge-info">File Type: png, ico</span><span>@if($errors){{$errors->first('site_logo') }}@endif</span>
                              </div>
                              <div class="form-group col-sm-6">
                                <label>Government Logo</label>
                                <input type="file" class="form-control" name="govt_logo" accept="image/*">
                                <span class="badge badge-info">File Type: jpg, jpeg, png</span><span>@if($errors){{$errors->first('govt_logo') }}@endif</span>
                              </div>
                             
                             
                          </div>                          
                       <center><button class="btn bg-gradient-success btn-flat col-sm-2"><i class="fas fa-save"></i> 
                    Update Settings</button></center> 
                    
                </form>
            </div>
            <!-- /.card -->
        </div>

    </div>
    <!--/. container-fluid -->
</section>
@endsection

@section('script')
<script type="text/javascript">
  $(function(){
      $('#btn-toastr').on('click',function(){
        toastr.success("","Great job! You click a toastr success notification.");
      });

      //Sweet alert notification settings
      const swal = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
      });

      $('#btn-swal').on('click',function(){
          swal.fire({
              type  : 'info',
              title : 'Great job! You click a swal info notification.'
          });
      });

      $('#btn-notify').on('click',function(){
          $.notify('Great job! You click a notify js notification.','info');
      });


  });    
</script>
@endsection
