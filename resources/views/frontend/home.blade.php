@extends('frontend.layouts.index') @section('content')
<!-- slider -->
@include('frontend.layouts.slider')
<!--slider-->

<!-- notice -->
@include('frontend.layouts.news')
<!-- notice -->

<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
        <!-- Left bar -->
        @include('frontend.layouts.leftbar')
        <!--Left bar-->
        <div class="mid_blog_box">
            <div class="main_mid_blog">
                <div class="top_blog_head">
                    <h1>{{ $introduction->introduction_title }}<br/>
                                </h1>

                    <p>{{ $introduction->introduction_description }}</p>

                </div>
                <!--top_blog_head-->

            </div>
            <!--main_mid_blog-->
            @foreach($news as $value)
            <div class="main_min_boxs">
                <h1 style="float:  left;     float: left; padding-left: 7px;padding-top: 20px;margin-bottom:10px; width: 415px;">{{$value->news_title}}</h1>
                <div class="full_oo">
                    <div class="full_oo_left">

                        <span style="padding-left: 0px; color: #000;">Published: {{ $value->news_date }}</span>

                    </div>

                </div>
                <img src="{{ asset('public/uploads/news/'.$value->news_image) }}" onerror="handleImgError(this)" alt="image" height="280" width="464" />
                <p>{{ $value->news_short }}.....</p>
                <div class="full_oo">
                    <div class="full_oo_left">
                        <span>Posted By: </span><b style="font-size: 15px;">&nbsp; spweb</b>
                        <br />

                    </div>
                    <a href="{{ route('detail.news',$value->id) }}" style="text-decoration: underline; font-size: 13px;"><img src="{{ asset('public/frontend/images/deta.png') }}" style="width: 54px; height: 16px;border:none;" /></a>

                </div>
            </div>

            @endforeach
            <!--full_oo-->

        </div>
        <!--mid_blog_box-->
        <div class="right_bar">
            {{--
            <div class="sidebar"> --}}
                <div class="sidebar_sec" style="margin-top: 0px;">

                    <h1>কি সেবা কি ভাবে পাবেন?</h1>
                    <div class="news_move">

                        <span style="font-size: 17px;"><h5 align="justify"><font face="helvetica" size="6"><b>অত্র পুলিশ সুপারেরকার্যালয় <br>হতে সরাসরি যে সকল সেবা সমূহ পাবেনঃ-</b></font></h5><div align="justify">ক) যে কোন আইনানুগ সহযোগিতা বা পরামর্শ পাবেন।<br>খ) অফিস চলাকালীন সময়ে মাননীয় পুলিশ সুপারের সঙ্গে সাক্ষাৎ করার সুবিধা।<br>গ) থানা/পুলিশ ফাঁড়ি/পুলিশ তদন্ত কেন্দ্রে মামলা/জিডি গ্রহনে অনীহা প্রকাশ করলে বিষয়টি অত্রদপ্তরের প্রধানকে অবহিত করলে আইনগত সহযোগিতা পাবেন।<br>ঘ) অত্র জেলার ভিতরে যে কোন স্থানে আইন-শৃংখলার অবনতি ঘটলে বা কোন মারাত্বক দাঙ্গা হাঙ্গামা হওয়ার সম্ভাবনা থাকিলে&nbsp;&nbsp;&nbsp; উক্ত স্থানে পুলিশ ক্যাম্পের প্রয়োজনীয়তা অনুভব করলে মাননীয় পুলিশ সুপার মহোদয় বরাবর আবেদন করুন।<br>ঙ) আপনার কোন সামাজিক/সাংস্কৃতিক অনুষ্ঠানে নিরাপত্তার লক্ষ্যে পুলিশ ফোর্সের প্রয়োজনীয়তা দেখা দিলে মাননীয় পুলিশ সুপার, জেলা বিশেষ শাখা, লক্ষীপুর মহোদয় বরাবর আবেদন করুন।<br></div><br><br></span>
                    </div>
                    <!--news move-->

                </div>
                <!--sidebar-->
                <!--sidebar-->
                <div class="sidebar_sec">
                    <h1>প্রয়োজনীয় কয়েকটি লিঙ্কস</h1>
                    <ul>
                        @foreach($link as $value)
                        <li><a href="{{ $value->link_address }}" target="_blank">{{ $value->link_name }}</a></li>
                        @endforeach

                        <a href="{{ route('site.necessary.link.detail') }}" class="deta"><img src="{{ asset('public/frontend/images/deta.png') }}" alt="button" /></a>

                    </ul>
                </div>
                <!--sidebar_sec-->

                <div class="sidebar_sec" style="margin-bottom:15px;">
                    <h1>লক্ষ্মীপুরের   মানচিত্র</h1>

                    <div class="maps"><img src="{{ asset('public/frontend/images/map_07.gif') }}" alt="map" /></div>

                </div>
                <!--sidebar_sec-->
                <div class="sidebar_sec">
                    <h1>Most Wanted</h1>
                    <ul>

                        <img src="{{ asset('public/frontend/images/wanted/.html') }} " onerror="handleImgError(this)" width="224" height="224" alt="" />
                        <br>

                        <h2 style="text-align: center; margin-top: 8px;"></h2>

                        <p style="text-align: justify; margin-top: 5px;"></p>

                        <br>
                        <a href="{{ route('criminal.list') }}"  alt="button">View All</a>
                    </ul>

                    <!--sidebar_sec-->

                </div>
                <!--sidebar-->
            </div>
            <!--right_bar--->
        </div>
        <!--content_blog-->
        @include('frontend.layouts.footer')
    </div>
    <!--main_content_blog-->
    @endsection