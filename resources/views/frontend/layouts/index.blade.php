<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <link href="{{asset('public/frontend/css/style.css')}}" type="text/css" rel="stylesheet" />
    <!--slider-->
    <script type="text/javascript" src="{{ asset('public/frontend/js/slider/jquery.min.js') }}"></script>
</head>

<body>
    <div id="wrapper">
        <div id="main_blog">
            <div id="header">
                <div class="logo">
                    <a href="#"><img src="{{ asset('public/uploads/logo/site_logo/'.$data->site_logo) }}" width="117" height="118" alt="logo" /></a>
                </div>
                <div class="mid_blog"><img src="{{ asset('public/frontend/images/poli-head.png') }} " alt="police head img" /></div>
                <div class="gov_logo"><img src="{{ asset('public/uploads/logo/govt_logo/'.$data->govt_logo) }}" width="115" height="117" alt="gov-logo" /></div>

            </div>
            <!--header-->

            <!-- navbar -->
            @include('frontend.layouts.navbar')
            <!-- navbar -->


            <div id="container">
                @yield('content')
            </div>
            <!--conatiner-->
        </div>
        <!--main_blog-->

    </div>
    <!--Wrapper-->
    <center>
        <div style="width: 400px; margin: auto; overflow: hidden;">
            <span style="color: #000;margin-top: 37px;display: block; font-size: 14px;"> <p style="  float: left; margin-left: 113px;margin-top: 18px;">Powered By</p> <a href="http://www.dynamicsoftwareltd.com/" target="_blank" style="color: #000;"><img src="{{ asset('public/frontend/images/pow.png') }} " height="50" width="180" />   </a></span>
        </div>

    </center>
    <script type="text/javascript" src="{{ asset('public/frontend/js/slider/jquery.cycle.all.2.74.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.slideshow').cycle({
                fx: 'fade'
            });
        });
    </script>
    <!--slider-->
    <script type="text/javascript">
        function handleImgError(srcElement) {
            srcElement.src = "{{ asset('public/frontend/images/no.jpg') }}";
            srcElement.onerror = "";
            return true;
        }
    </script>
</body>
</html>