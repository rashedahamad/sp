<div class="left_bar">

    <div class="sidebar_sec" style="margin-top: 0px;">
        <h1>{{ $statement->police_statement_title }}</h1>

        <div class="super_mess">
            <!--  <div class="comme">Commisioner message</div> -->
            <div class="pic_comm"><img src="{{ asset('public/uploads/policeStatement/'.$statement->police_image) }} " onerror="handleImgError(this)" width="189" height="231" alt="PIC" /></div>
            <div class="name_b">
                <!--    <h4>Benazir Ahmed BNP</h4>
                                        <span>Police commissioner</span>
                                        <p style="text-align:center;">Message of commissioner</p>-->

            </div>
            <!--name_b-->
        </div>
        <!--sidebar-->
        <p class="paraa">{{ $statement->police_short_description }}</p>

        <a href="{{ route('police.statement.details') }}" class="deta"><img src="{{asset('public/frontend/images/deta.png')}}" alt="button" /></a>
    </div>

    <div class="sidebar_sec">
        <h1>Our Vision</h1>

        <div class="parss">

            <span style="font-size: 15px;">
                       <span class="Apple-style-span" style="font-size: 15px;"><span style="font-size:12.0pt;font-family:SutonnyMJ;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">{{ $mission_vision->vision }}<span style="mso-spacerun: yes">&nbsp;
</span><span style="mso-spacerun: yes"> </span>
            </span>
            <br>
            </span>
            <br> </span>

        </div>

    </div>

    <!--sidebar_sec-->

    <div class="sidebar_sec">
        <h1>Our Mission</h1>

        <div class="parss">

            <span class="Apple-style-span" style="font-size: 15px;"><span style="font-size:12.0pt;font-family:SutonnyMJ;
mso-fareast-font-family:&quot;Times New Roman&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-ansi-language:EN-US;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">{{ $mission_vision->mission }}<span style="mso-spacerun: yes">&nbsp;
</span><span style="mso-spacerun: yes"> </span>

        </div>

    </div>
    <!--sidebar_sec-->
    <!--sidebar_sec-->

    <div class="sidebar_sec">

        <h1>Notice</h1>
        @foreach($notice as $value)
        <h2 style="margin-top: 30px; font-size: 20px; ">{{ $value->notice_title }} </h2>

        <a href="{{ route('site.notice.detail',$value->id) }}" class="deta"><img src="{{asset('public/frontend/images/deta.png')}}" alt="button" /></a>
        @endforeach
        

    </div>
    <!--sidebar_sec-->

    <div class="sidebar_sec">
        <h1>Online Visitor</h1>
        <!-- BEGIN: Powered by Supercounters.com -->
        <center>
            <script type="text/javascript" src="../widget.supercounters.com/flag.js"></script>
            <script type="text/javascript">
                sc_flag(604180, "FFFFFF", "000000", "cccccc", 2, 20, 0, 0)
            </script>
            <br>
            <noscript><a href="http://www.supercounters.com/">Flag Counter</a></noscript>
        </center>
        <!-- END: Powered by Supercounters.com -->

    </div>
    <!--sidebar_sec-->
    <div class="sidebar_sec">
        <h1>আবহাওয়ার খবর</h1>
        <span style="float: right; margin-right: 5px;">  

                     <script language="JavaScript" type="text/javascript">
                         document.write('<script language="JavaScript" src="http://www.worldweatheronline.com/widget/v2/weather-widget.ashx?locid=181809&root_id=165624&wc=LOCAL_WEATHER&map=~/lakshmipur-weather-widget/bd.aspx&width=200&custom_header=Lakshmipur Weather, Bangladesh&num_of_day=1&title_bg_color=020202&title_text_color=FFFFFF&widget_bg_color=FFFFFF&widget_text_color=020202&type=js&cb=' + Math.random() + '" type="text/javascript"><\/scr' + 'ipt>');
                     </script><noscript><a href="http://www.worldweatheronline.com/"
                                           alt="7 day Lakshmipur Weather, Bangladesh weather">7 day Lakshmipur Weather,
                                    Bangladesh weather</a> provided by <a href="http://www.worldweatheronline.com/">World
                                    Weather Online</a></noscript>

                        </span>
    </div>
</div>