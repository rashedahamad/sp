<div style="width: 100%; position: ; z-index: 1000;">
    <ul class="menuTemplate2 decor2_1" license="mylicense" style="margin-top: 13px;  width: 1068px;">

        <li><a href="{{ route('homepage') }}" class="arrow">প্রচ্ছদ</a></li>
        <li class="separator"></li>

        <!--start-->
        <li>
            <a href="{{ route('district.police') }}" class="arrow">লক্ষীপুর জেলা পুলিশ</a>

            <div class="drop decor2_2" style="width: 640px;">

                <div class="left">

                    <div>

                        <a href="{{ route('site.manpower') }}">জনবল এবং নিয়োগ সম্পকিৃত তথ্য</a>
                        <br/>
                        <a href="{{ route('promotion.transfer.trainging') }}">পদোন্নতি/স্থানান্তর/প্রশিক্ষণ</a>
                        <br/>
                        <a href="{{ route('citizen.chart') }}">সিটিজেন চার্টার</a>

                        <br/>
                        <a href="{{route('information.provider.officers')}}">তথ্য প্রদান কারী কমর্কর্তা</a>

                        <br/>
                        <a href="{{ route('previous.police.super') }}">প্রাক্তন পুলিশ সুপার</a>

                        <br/>

                        <a href="{{ route('site.police.officers') }}">কর্মকর্তা</a>
                        <br/>

                        <a href="{{ route('police.staff') }}">কর্মচারী</a>
                        <br/>

                        <a href="{{ route('police.help') }}">সাহায্য</a>

                    </div>

                </div>

                <div style="clear: both;"></div>
            </div>
        </li>
        <!--End-->
        <!--start-->
        <li>
            <a href="{{ route('emergency.communication') }}" class="arrow">যোগাযোগ</a>

            <div class="drop decor2_2" style="width: 640px;">

                <div class="left">

                    <div>

                        <a href="{{ route('emergency.communication') }}">জরুরী যোগাযোগ</a>
                        <br/>
                        <a href="{{ route('weblink.police') }}">পুলিশ সম্পর্কিত ওয়েব লিংক</a>
                        <br/>
                        <a href="{{ route('telephone.guide') }}">লক্ষ্মীপুর জেলা পুলিশ টেলিফোন গাইড</a>
                        <br/>
                        <!-- <a href="weblink2.php">&#x0993;&#x09DF;&#x09C7;&#x09AC;&nbsp;&nbsp;&nbsp;&#x09B8;&#x09BE;&#x0987;&#x099F;&nbsp;&nbsp;&nbsp;&#x09B2;&#x09BF;&#x0982;&#x0995;</a>-->

                    </div>

                </div>

                <div style="clear: both;"></div>
            </div>
        </li>
        <!--End-->
        <!--start-->
        <li>
            <a class="arrow">
                ইউনিট সমূহ</a>

            <div class="drop decor2_2" style="width: 447px;">
                <div class="left">
                    <div>
                        <a href="{{ route('unite.image') }}">ইউনিট সমূহের ছবি</a>
                        <br/>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </li>
        <!--End-->

        <!--start-->
        <li>
            <a href="{{ route('crime.complain') }}" class="arrow">পাবলিক সেক্টর</a>

            <div class="drop decor2_2" style="width: 640px;">

                <div class="left">

                    <div>

                        <a href="{{ route('crime.complain') }}">অভিযোগ/অপরাধ বিষয়ে অবহিত করন/পরামর্শ</a>
                        <br/>
                        <a href="{{ route('crime.complain') }}">আবেদন পত্রের অবস্থা</a>
                        <br/>
                        <a href="{{ route('sms.information') }}">
                            এসএমএস</a>
                        <br/>
                        <a href="{{ route('safty.tips') }}">
                            সর্তকর্তা</a>
                        <br/>
                        <a href="{{ route('criminal.list') }}">ক্রিমিনাল তালিকা</a>
                        <br/>
                    </div>

                </div>

                <div style="clear: both;"></div>
            </div>
        </li>
        <!--End-->

        <!--start-->
        <li>
            <a href="{{ route('photo.gallery') }}" class="arrow">ফটো গ্যালারী</a>

        </li>
        <li>
            <a href="{{ route('download.document') }}" class="arrow">ডাউনলোড</a>

        </li>
        <li>
            <a href="{{ route('safty.tips') }}" class="arrow">নিরাপত্তা টিপস</a>

        </li>
        <li>
            <a class="arrow">নোটিশ বোর্ড</a>
            <div class="drop decor2_2" style="width: 120px;">
                <div class="left">
                    <div>
                        <a href="{{ route('site.notice.view') }}" class="arrow">নোটিশ</a>
                        <a href="{{ route('site.noc.view') }}" class="arrow">NOC</a>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </li>
        <li>
            <a href="{{ route('news') }}" class="arrow" style="float: right; display: inline;">খবর</a>

        </li>
        <!--End-->

        <li class="separator"></li>

        <!--  <li>

          <a href="#Horizontal-Menus" class="arrow">Horizontal Menus</a>
           <div class="drop decor2_2" style="width: 640px;">

               <div class='left'>

                <div>

                <a href="#">Search engine friendly</a><br />
               <a href="#">Best css menu</a><br />

                 <a href="#">Menu Design Templates</a>

                   </div>

                  </div>

                   <div style='clear: both;'></div> </div> </li>
           <li class="separator"></li>-->

    </ul>
</div>