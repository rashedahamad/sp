@extends('frontend.layouts.index') 
@section('content')
<!-- slider -->
<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
@include('frontend.layouts.leftbar')
<div class="tsos">
                        <h1>NOC List</h1>
                        <hr>
                        <table>
                            <tr style="font-size: 17px;">
                                <th > নাম</th>
                                <th >পিতার নাম</th>
                                <th >ঠিকানা</th>
                                <th >পিকচার</th>
                            </tr>
                            @foreach($noc as $nocs)
                                                            <tr style="font-size: 15px;">
                                    <td>{{$nocs->noc_name }}</td>
                                    <td>{{ $nocs->noc_father_name  }}</td>
                                    <td>{{ $nocs->noc_address }}</td>
                                    <td><img src="{{ asset('public/uploads/noc/'.$nocs->noc_image) }}" alt="NO Image" height="50">

                                        <p style="font-weight: bold">
                                            <a href="{{ asset('public/uploads/noc/'.$nocs->noc_image) }}" style="text-decoration: underline;" download>Download</a></p>

                                    </td>
                                    @endforeach
                                </tr>
                                                    </table>

                    </div>
</div>
</div>


@endsection