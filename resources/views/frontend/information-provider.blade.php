@extends('frontend.layouts.index') @section('content')
<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
        <!-- Left bar -->
        @include('frontend.layouts.leftbar')
        <div class="tsos">
            <h1>তথ্য প্রদানকারী কমর্কর্তা</h1>
            <hr>
            <table>
                <tr style="font-size: 17px;">
                    <th>ক্রমিক নং</th>
                    <th>কর্মকর্তার তথ্য</th>
                    <th>ছবি</th>
                </tr>
                @foreach($information_provider as $value)
                <tr style="font-size: 15px;">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $value->information_provider_name }}
                        <div>{{ $value->information_provider_designation }} (সদর)</div>
                        <div>{{ $value->information_provider_address }}।</div>
                    </td>
                    
                    <td><img src="{{asset('public/uploads/informationProvider/'.$value->information_provider_image)}}" alt="NO Image" height="150">
                    </td>
                </tr>
                @endforeach
            </table>

        </div>

    </div>
</div>

@endsection