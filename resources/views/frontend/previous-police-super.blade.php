@extends('frontend.layouts.index') @section('content')
<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
        <!-- Left bar -->
        @include('frontend.layouts.leftbar')
        <div class="tsos">
            <h1>প্রাক্তন পুলিশ সুপার</h1>

            <br />


            @foreach($statement_all as $value)
            <div class="sidebar_sec" style="margin-top: 0; height:380px;">
                <div class="super_mess">
                    <div class="pic_comm"><img src="{{ asset('public/uploads/policeStatement/'.$value->police_image) }}" onerror="handleImgError(this)" width="189" height="231" alt="PIC" /></div>
                </div>
                <!--sidebar-->
                <h2>{{ $value->police_name }}</h2>
                <h2>Date:{{ $value->police_joining_date }} to {{$value->police_end_date  }}</h2>
            </div>
            @endforeach

        </div>

    </div>
</div>
@endsection