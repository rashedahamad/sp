@extends('frontend.layouts.index') @section('content')
<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
        <div class="tsos" style="width: 100%; margin: auto;">
            <h1>&#x09AB;&#x099F;&#x09C7;&#x09BE;&nbsp;&#x0997;&#x09CD;&#x09AF;&#x09BE;&#x09B2;&#x09BE;&#x09B0;&#x09C0; </h1> 
            <div class="gallery_boxx">
                @foreach($gallery as $value)
                <div class="gallery" style="width: 300px; float:left; border:3px solid #f8f8fe; margin: 0 10px">
                    <img src="{{ asset('public/uploads/photoGallery/'.$value->image)}}" width="290" height="345" alt=""/>
                </div>
                @endforeach
                <ul id="pag_box">

                </ul>
            </div>
            
        </div>
    </div>
</div>
@endsection