@extends('frontend.layouts.index') @section('content')
<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
        <!-- Left bar -->
        @include('frontend.layouts.leftbar')

        <div class="tsos">
            <h1>পুলিশকর্মকর্তা</h1> @foreach($officer as $value)
            <div class="sidebar_sec" style="margin-top: 5px; height:350px;">
                <div class="super_mess">
                    <!--  <div class="comme">Commisioner message</div> -->
                    <div class="pic_comm"><img src="{{ asset('public/uploads/officer/'.$value->police_officer_image) }}" onerror="handleImgError(this)" onerror="handleImgError(this)" width="189" height="231" alt="PIC" /></div>
                    <div class="name_b">
                        <!--    <h4>Benazir Ahmed BNP</h4>
                                <span>Police commissioner</span>
                                <p style="text-align:center;">Message of commissioner</p>-->

                    </div>
                    <!--name_b-->
                </div>
                <!--sidebar-->
                <h3 style="margin-left: 7px; font-size: 14px; margin-bottom: 6px;">{{ $value->police_officer_name }}    </h3>
                <h3 style="margin-left: 7px; font-size: 14px; margin-bottom: 5px;">পদবী : {{ $value->police_officer_designation }} {{ $value->police_officer_devission }}</h3>

            </div>
            @endforeach

        </div>

    </div>
</div>
@endsection