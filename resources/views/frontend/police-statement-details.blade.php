@extends('frontend.layouts.index') @section('content')
<div id="container">

    <div class="main_content_blog" style="min-height: 2143px;">
        <div class="content_blog">
            <div class="tsos" style="width: 100%; margin: auto;">
                <h1>{{ $statement->police_statement_title }}</h1>
                <div class="kfndsfkd" style="border: 1px solid #333; width: 98%;">

                    <table style="width: 100%; margin-top: 15px;">
                        <div style="margin: 10px auto; width:200px ; height: 252px;"><img src="{{ asset('public/uploads/policeStatement/'.$statement->police_image) }}" width="200" height="252" /></div>

                        <span style="  font-size: 23px;overflow: hidden;padding: 6px;text-align: justify;width: 913px;">
          <div align="justify">{{$statement->police_long_description}}<br><br></div><div align="center"><b>{{ $statement->police_name }}</b><br>{{ $statement->police_designation }}<br>লক্ষ্মীপুর ।<br></div>          </span>
                    </table>

                </div>
            </div>

        </div>
        <!--content_blog-->

    </div>
    <!--main_content_blog-->

</div>

@endsection