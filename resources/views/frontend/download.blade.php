@extends('frontend.layouts.index') @section('content')
<div class="main_content_blog" style="height: ;">
    <div class="content_blog">
        <div class="tsos" style="width: 100%; margin: auto;">
            <h1>ডাউনলোড</h1>
            <div class="kfndsfkd">

                <table style="width: 100%; font-size: 25px; margin-top: 15px;">

                    <tr style="background: ;">
                        <td><b>নাম</b></td>

                        <td><b>প্রকাশের তারিখ</b></td>
                        <td><b>ডাউনলোড</b></td>

                    </tr>
                    @foreach($download as $value)
                    <tr style="background: #fff; margin-top: 10px:;">
                        <td>{{ $value->name }}</td>

                        <td>{{ $value->publish_date }}</td>
                        <td><a href="{{ asset('public/uploads/document/'.$value->file) }}" style="text-decoration: underline;" download>Download</a></td>
                    </tr>
                    @endforeach

                    
                </table>

            </div>
        </div>
    </div>
</div>
@endsection