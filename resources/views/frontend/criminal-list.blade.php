@extends('frontend.layouts.index')
 @section('content')
 <div class="main_content_blog" style="height: ;">
    <div class="content_blog">
 @include('frontend.layouts.leftbar')
 <div class="tsos">
               <h1>ক্রিমিনাল এর তালিকা</h1>
				   <hr>
               					<table>
					  <tr style="font-size: 17px;">
						<th>ছবি</th>
						<th > নাম</th>
						<th >পিতার নাম</th>
						<th >স্থায়ী ঠিকানা</th>
						<th >বর্ণনা</th>
					  </tr>
					  @foreach($criminal as $value)
					  					  <tr style="font-size: 15px;">
						<td><img src="{{ asset('public/uploads/criminal/'.$value->criminal_image) }}" onerror="handleImgError(this)" width="80" height="100" alt=""/></td>

						<td>{{ $value->criminal_name }}</td>
						<td>{{ $value->criminal_father }}</td>
						<td>{{ $value->criminal_address }}</td>
						<td>{{ $value->criminal_description }}</td>
					  </tr>
					  @endforeach
										 
										</table>

               </div>

</div>
</div>
@endsection