<?php

use App\Http\Controllers\Backend\CriminalListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

		Route::get('/locale/{lang}', function($lang) {
			Session::put('locale', $lang);
		    return redirect()->back();
		})->name('locale');

		//FrontEnd //
		Route::get('/','Frontend\FrontController@index')->name('homepage');
		//News
		Route::get('/news','Frontend\FrontController@showNews')->name('news');
		Route::get('/news/detail/{id}','Frontend\FrontController@detailNews')->name('detail.news');
		//Notice
		Route::get('/notice','Frontend\FrontController@showNotice')->name('site.notice.view');
		Route::get('site/notice/detail/{id}','Frontend\FrontController@detailNotice')->name('site.notice.detail');

		//NOC
		Route::get('/notice/noc','Frontend\FrontController@showNoc')->name('site.noc.view');
		//Necessary links

		Route::get('/site/necessary/link/details','Frontend\FrontController@showLinkDetail')->name('site.necessary.link.detail');
		//Manpower and recruitment information
		Route::get('/site/manpower','Frontend\FrontController@showManPower')->name('site.manpower');
		//Citizen Chart
		Route::get('/citizen/chart','Frontend\FrontController@showCitizenChart')->name('citizen.chart');
		//Emergency communication
		Route::get('/emergency/communication','Frontend\FrontController@showEmergencyCommunication')->name('emergency.communication');

		//Police Statement
		Route::get('/police/statement/details','Frontend\FrontController@showStatementDetails')->name('police.statement.details');
		//SaftyTips
		Route::get('/safty/tips','Frontend\FrontController@SaftyTips')->name('safty.tips');
		//web link to the police
		Route::get('/weblink/police','Frontend\FrontController@WebLinkPolice')->name('weblink.police');
		//Police Telephone Guide
		Route::get('/telephone/guide','Frontend\FrontController@TelephoneGuide')->name('telephone.guide');
		//Promotion/transfer/training
		Route::get('/promotion/transfer/training','Frontend\FrontController@PromotionTransferTraining')->name('promotion.transfer.trainging');

		//Information provider
		Route::get('/information/provider/officers','Frontend\FrontController@InformationProvider')->name('information.provider.officers');

		//Former police superintendent
		Route::get('/previous/police/super','Frontend\FrontController@PreviousPoliceSuper')->name('previous.police.super');
		//Officers
		Route::get('site/police/officers','Frontend\FrontController@PoliceOfficers')->name('site.police.officers');
		//Staff
		Route::get('/police/staff','Frontend\FrontController@PoliceStaff')->name('police.staff');
		//help
		Route::get('/police/help','Frontend\FrontController@PoliceHelp')->name('police.help');
		//unite Picture
		Route::get('/unite/image','Frontend\FrontController@UniteImage')->name('unite.image');
		//Crime Complain
		Route::get('/crime/complain','Frontend\FrontController@CrimeComplain')->name('crime.complain');
		//SMS Information
		Route::get('/sms/information','Frontend\FrontController@SmsInformation')->name('sms.information');
		//Criminal List
		Route::get('/criminal/list','Frontend\FrontController@CriminalList')->name('criminal.list');
		//Photo Gallery
		Route::get('/photo/gallery','Frontend\FrontController@PhotoGallery')->name('photo.gallery');
		//Download document
		Route::get('/download/document','Frontend\FrontController@Download')->name('download.document');
		//Zela Police
		Route::get('/district/police','Frontend\FrontController@DistrictPolice')->name('district.police');
		//Mission And Vission
		// Route::get('/site/mission/vission','Frontend\FrontController@MissionVission')->name('view.mission.vission');
















Auth::routes();

Route::middleware(['auth'])->group(function(){
	Route::get('/home', 'Backend\HomeController@index')->name('dashboard');	
	
	Route::prefix('site')->group(function(){
		Route::get('settings','Backend\SiteSettingController@index')->name('site.setting');		
		Route::get('slider','Backend\SliderController@index')->name('site.slider');
		Route::get('slider/add','Backend\SliderController@addSlider')->name('site.slider.add');
		Route::post('slider/store','Backend\SliderController@storeSlider')->name('site.slider.store');
		Route::get('slider/delete/{id}','Backend\SliderController@deleteSlider')->name('site.slider.delete');
		Route::get('slider/edit/{id}','Backend\SliderController@editSlider')->name('site.slider.edit');
		Route::post('slider/update/{id}','Backend\SliderController@updateSlider')->name('site.slider.update');

		// Route::post('slider/logo/store','Backend\SiteSettingController@storeLogo')->name('add.site.logo');
		Route::post('slider/logo/store/{id}','Backend\SiteSettingController@storeLogo')->name('add.site.logo');

		//l
		Route::get('site/news/view','Backend\NewsController@viewNews')->name('view.site.news');
		Route::get('site/news/add','Backend\NewsController@addNews')->name('add.site.news');
		Route::post('site/news/store','Backend\NewsController@storeNews')->name('store.site.news');
		Route::get('site/news/edit/{id}','Backend\NewsController@editNews')->name('edit.site.news');
		Route::post('site/news/update/{id}','Backend\NewsController@updateNews')->name('update.site.news');
		Route::get('site/news/delete/{id}','Backend\NewsController@deleteNews')->name('delete.site.news');

		//Introduction
		Route::get('site/introduction','Backend\IntroductionController@viewIntro')->name('view.introduction');
		Route::get('site/introduction/add','Backend\IntroductionController@addIntro')->name('add.introduction');
		Route::post('site/introduction/update/{id}','Backend\IntroductionController@updateIntro')->name('update.introduction');
		Route::get('site/introduction/edit/{id}','Backend\IntroductionController@editIntro')->name('edit.introduction');


		//Notice
		Route::get('site/notice/','Backend\NoticeController@showNotice')->name('site.notice');
		Route::get('site/notice/add','Backend\NoticeController@addNotice')->name('site.notice.add');
		Route::post('site/notice/store','Backend\NoticeController@storeNotice')->name('site.notice.store');
		Route::get('site/notice/edit/{id}','Backend\NoticeController@editNotice')->name('site.notice.edit');
		Route::get('site/notice/delete/{id}','Backend\NoticeController@deleteNotice')->name('site.notice.delete');
		Route::post('site/notice/update/{id}','Backend\NoticeController@updateNotice')->name('site.notice.update');
		
		//Noc

		Route::get('site/noc','Backend\NocListController@showNoc')->name('site.noc');
		Route::get('site/noc/add','Backend\NocListController@addNoc')->name('site.noc.add');
		Route::post('site/noc/store','Backend\NocListController@storeNoc')->name('site.noc.store');
		Route::get('site/noc/edit/{id}','Backend\NocListController@editNoc')->name('site.noc.edit');
		Route::get('site/noc/delete/{id}','Backend\NocListController@deleteNoc')->name('site.noc.delete');
		Route::post('site/noc/update/{id}','Backend\NocListController@updateNoc')->name('site.noc.update');

		//NecessaryLink
		Route::get('site/necessary/link','Backend\NecessaryLinkController@showNecessaryLink')->name('site.necessary.link');
		Route::get('site/necessary/link/add','Backend\NecessaryLinkController@addNecessaryLink')->name('site.necessary.link.add');
		Route::post('sitenoc/store','Backend\NecessaryLinkController@storeNecessaryLink')->name('site.necessary.link.store');
		Route::get('site/necessary/link/edit/{id}','Backend\NecessaryLinkController@editNecessaryLink')->name('site.necessary.link.edit');
		Route::get('site/necessary/link/delete/{id}','Backend\NecessaryLinkController@deleteNecessaryLink')->name('site.necessary.link.delete');
		Route::post('site/necessary/link/update/{id}','Backend\NecessaryLinkController@updateNecessaryLink')->name('site.necessary.link.update');

		//Superintendent of Police Statement

		Route::get('site/police/statement','Backend\PoliceStatementController@showPoliceStatement')->name('site.police.statement.show');
		Route::get('site/police/statement/add','Backend\PoliceStatementController@addPoliceStatement')->name('site.police.statement.add');
		Route::post('site/police/statement/store','Backend\PoliceStatementController@storePoliceStatement')->name('site.police.statement.store');
		Route::get('site/police/statement/edit/{id}','Backend\PoliceStatementController@editPoliceStatement')->name('site.police.statement.edit');
		
		Route::post('site/police/statement/update/{id}','Backend\PoliceStatementController@updatePoliceStatement')->name('site.police.statement.update');

		//Mission and Vission
		Route::get('mission/vision','Backend\MissionVisionController@showMissionVision')->name('mission.vision.show');
		Route::get('mission/vision/add','Backend\MissionVisionController@addMissionVision')->name('mission.vision.add');
		Route::post('mission/vision/store','Backend\MissionVisionController@storeMissionVision')->name('mission.vision.store');
		Route::get('mission/vision/edit/{id}','Backend\MissionVisionController@editMissionVision')->name('mission.vision.edit');
		
		Route::post('mission/vision/update/{id}','Backend\MissionVisionController@updateMissionVision')->name('mission.vision.update');
		Route::get('mission/vision/delete/{id}','Backend\MissionVisionController@deleteMissionVision')->name('mission.vision.delete');

		//Information Provider Officers
		Route::get('information/provider','Backend\InformationProviderController@showInformationProvider')->name('information.provider.show');
		Route::get('information/provider/add','Backend\InformationProviderController@addInformationProvider')->name('information.provider.add');
		Route::post('information/provider/store','Backend\InformationProviderController@storeInformationProvider')->name('information.provider.store');
		Route::get('information/provider/edit/{id}','Backend\InformationProviderController@editInformationProvider')->name('information.provider.edit');
		
		Route::post('information/provider/update/{id}','Backend\InformationProviderController@updateInformationProvider')->name('information.provider.update');
		Route::get('information/provider/delete/{id}','Backend\InformationProviderController@deleteInformationProvider')->name('information.provider.delete');

		//Police Officers
		Route::get('police/officer','Backend\PoliceOfficerController@showPoliceOfficer')->name('police.officer.show');
		Route::get('police/officer/add','Backend\PoliceOfficerController@addPoliceOfficer')->name('police.officer.add');
		Route::post('police/officer/store','Backend\PoliceOfficerController@storePoliceOfficer')->name('police.officer.store');
		Route::get('police/officer/edit/{id}','Backend\PoliceOfficerController@editPoliceOfficer')->name('police.officer.edit');
		
		Route::post('police/officer/update/{id}','Backend\PoliceOfficerController@updatePoliceOfficer')->name('police.officer.update');
		Route::get('police/officer/delete/{id}','Backend\PoliceOfficerController@deletePoliceOfficer')->name('police.officer.delete');
		
		//Police Staffs

		Route::get('police/staff','Backend\PoliceStaffController@showPoliceStaff')->name('police.staff.show');
		Route::get('police/staff/add','Backend\PoliceStaffController@addPoliceStaff')->name('police.staff.add');
		Route::post('police/staff/store','Backend\PoliceStaffController@storePoliceStaff')->name('police.staff.store');
		Route::get('police/staff/edit/{id}','Backend\PoliceStaffController@editPoliceStaff')->name('police.staff.edit');
		
		Route::post('police/staff/update/{id}','Backend\PoliceStaffController@updatePoliceStaff')->name('police.staff.update');
		Route::get('police/staff/delete/{id}','Backend\PoliceStaffController@deletePoliceStaff')->name('police.staff.delete');
		
		//Criminal list

		Route::get('criminal/list/show','Backend\CriminalListController@showCriminalList')->name('criminal.list.show');
		Route::get('criminal/list/add','Backend\CriminalListController@addCriminalList')->name('criminal.list.add');
		Route::post('criminal/list/store','Backend\CriminalListController@storeCriminalList')->name('criminal.list.store');
		Route::get('criminal/list/edit/{id}','Backend\CriminalListController@editCriminalList')->name('criminal.list.edit');
		
		Route::post('criminal/list/update/{id}','Backend\CriminalListController@updateCriminalList')->name('criminal.list.update');
		Route::get('criminal/list/delete/{id}','Backend\CriminalListController@deleteCriminalList')->name('criminal.list.delete');

		//Photo Gallery
		Route::get('photo/gallery/show','Backend\PhotoGalleryController@showPhotoGallery')->name('photo.gallery.show');
		Route::get('photo/gallery/add','Backend\PhotoGalleryController@addPhotoGallery')->name('photo.gallery.add');
		Route::post('photo/gallery/store','Backend\PhotoGalleryController@storePhotoGallery')->name('photo.gallery.store');
		Route::get('photo/gallery/edit/{id}','Backend\PhotoGalleryController@editPhotoGallery')->name('photo.gallery.edit');
		
		Route::post('photo/gallery/update/{id}','Backend\PhotoGalleryController@updatePhotoGallery')->name('photo.gallery.update');
		Route::get('photo/gallery/delete/{id}','Backend\PhotoGalleryController@deletePhotoGallery')->name('photo.gallery.delete');

		//Download
		Route::get('download/document/show','Backend\DownloadDocumentController@showDownloadDocument')->name('download.document.show');
		Route::get('download/document/add','Backend\DownloadDocumentController@addDownloadDocument')->name('download.document.add');
		Route::post('download/document/store','Backend\DownloadDocumentController@storeDownloadDocument')->name('download.document.store');
		Route::get('download/document/edit/{id}','Backend\DownloadDocumentController@editDownloadDocument')->name('download.document.edit');
		
		Route::post('download/document/update/{id}','Backend\DownloadDocumentController@updateDownloadDocument')->name('download.document.update');
		Route::get('download/document/delete/{id}','Backend\DownloadDocumentController@deleteDownloadDocument')->name('download.document.delete');


	});

	Route::prefix('tour-package')->group(function(){
		Route::get('view','Backend\SiteSettingController@index')->name('tour-package.view');
	});

});
