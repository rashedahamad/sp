<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PoliceStatement extends Model
{
    //
    protected $fillable = ['police_name','police_designation','police_joining_date','police_end_date','police_statement_title','police_short_description','police_long_description','police_image','serial'];
    
}
