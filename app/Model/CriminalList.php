<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CriminalList extends Model
{
    //
    protected $fillable = ['criminal_name','criminal_father','criminal_address','criminal_description','criminal_image'];
}
