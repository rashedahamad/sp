<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NocList extends Model
{
    //
    protected $fillable = ['noc_name','noc_father_name','noc_address','noc_image'];
}
