<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MissionVision extends Model
{
    //
    protected $fillable = ['mission','vision'];
}
