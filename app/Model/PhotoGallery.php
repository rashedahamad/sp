<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model
{
    //
    protected $fillable = ['title','image'];
}
