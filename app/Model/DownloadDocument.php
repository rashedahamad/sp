<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DownloadDocument extends Model
{
    //
    protected $fillable = ['name','publish_date','file'];
}
