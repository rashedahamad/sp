<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InformationProvider extends Model
{
    //
   
        protected $fillable = ['information_provider_name','information_provider_designation','information_provider_address','information_provider_image'];
}
