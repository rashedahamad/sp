<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $fillable = ['news_title','news_short','news_large','news_image','news_date'];
}
