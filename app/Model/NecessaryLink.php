<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NecessaryLink extends Model
{
    protected $fillable = ['link_name','link_address'];
}
