<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
	// $locale = App::
	protected $table = 'site_settings';
    // protected $fillable = [
    // 	'site_title',
    // 	'site_short_description',
    // 	'site_phone_primary',
    // 	'site_phone_secondary'
    // ];
    protected $fillable = ['site_logo','govt_logo'];
}
