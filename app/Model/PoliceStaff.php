<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PoliceStaff extends Model
{
    //
    protected $fillable = ['police_staff_name','police_staff_designation','police_staff_devission','police_staff_image'];
}
