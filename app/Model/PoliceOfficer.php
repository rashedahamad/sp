<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PoliceOfficer extends Model
{
	use SoftDeletes;
    //
    protected $fillable = ['police_officer_name','police_officer_designation','police_officer_devision','police_officer_image'];
}
