<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Introduction extends Model
{
    //
    protected $fillable = ['introduction_title','introduction_description'];
}
