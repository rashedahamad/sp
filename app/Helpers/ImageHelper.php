<?php
 
namespace App\Helpers;
 
class ImageHelper{
 
	static function uploadImage($config){
		$image 		= [];		
		$request 	= request();		

		$file = $request->file($config['name']);
		if($file){
			if (!is_dir($config['path'])){
			    mkdir($config['path']);       
			}

		}else{
			$image['status'] 	= false;
			$image['filename']	= '';
			$image['message']	= "Upload error. Please try again.";
		}
		
		return $image;
	}
}
