<?php namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Model\InformationProvider;
use App\Model\Introduction;
use App\Model\MissionVision;
use App\Model\NecessaryLink;
use App\Model\News;
use App\Model\NocList;
use App\Model\Notice;
use App\Model\PoliceOfficer;
use App\Model\PoliceStaff;
use App\Model\PoliceStatement;
use App\Model\SiteSetting;
use App\Model\Slider;
use App\Model\CriminalList;
use App\Model\PhotoGallery;
use App\Model\DownloadDocument;
use Illuminate\Http\Request;

class FrontController extends Controller {
    public function index() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $news=News:: all();
        $introduction=Introduction:: first();
        $link=NecessaryLink:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        return view('frontend.home', compact('data', 'sliderImg', 'news', 'introduction', 'link', 'statement','mission_vision','notice'));
    }
    public function showNews() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $news=News:: all();
        return view('frontend.news', compact('data', 'sliderImg', 'news'));
        // dd('Test');
    }
    public function detailNews($id) {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $detail_news=News:: find($id);
        return view('frontend.news-detail', compact('detail_news', 'data', 'sliderImg'));
    }
    public function showNotice() {
        // dd('test');
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $news=News:: all();
        $introduction=Introduction:: first();
        $notice=Notice:: all();
        return view('frontend.notice', compact('data', 'sliderImg', 'news', 'introduction', 'notice'));
        // dd('Test');
    }
    public function detailNotice($id) {
        // dd('test');
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $news=News:: all();
        $introduction=Introduction:: first();
        $notice=Notice::find($id);
        return view('frontend.notice-details', compact('data', 'sliderImg', 'news', 'notice'));
        // dd('Test');
    }
    public function showNoc() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $news=News:: all();
        $introduction=Introduction:: first();
        $notice=Notice:: all();
        $noc=NocList:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        return view('frontend.noc', compact('data', 'sliderImg', 'news', 'introduction', 'notice', 'noc', 'statement','mission_vision'));
    }
    public function showLinkDetail() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $link=NecessaryLink:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        return view('frontend.link-details', compact('data', 'sliderImg', 'link','statement'));
    }
    public function showManPower() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        return view('frontend.manpower', compact('data', 'sliderImg'));
    }
    public function showCitizenChart() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        return view('frontend.citizen-chart', compact('data', 'sliderImg','statement','mission_vision','notice'));
    }
    public function showEmergencyCommunication() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        return view('frontend.emergency-communication', compact('data', 'sliderImg'));
    }
    public function showStatementDetails() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $news=News:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        return view('frontend.police-statement-details', compact('data', 'sliderImg', 'news', 'statement'));
    }
     public function SaftyTips() {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        return view('frontend.safty-tips', compact('data', 'sliderImg', 'statement','mission_vision','notice'));
    }
    public function WebLinkPolice()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        return view('frontend.weblink-police', compact('data', 'sliderImg', 'statement','mission_vision','notice'));

    }
    public function TelephoneGuide()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        return view('frontend.telephone-guide-police', compact('data', 'sliderImg', 'statement'));

    }

    public function PromotionTransferTraining()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        return view('frontend.promotion-transfer-training', compact('data', 'sliderImg', 'statement','mission_vision','notice'));

    }
    public function InformationProvider()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $information_provider = InformationProvider::all();
        $notice = Notice::all();
        return view('frontend.information-provider', compact('data', 'sliderImg', 'statement','mission_vision','information_provider','notice'));

    }
    public function PreviousPoliceSuper()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $statement_all = PoliceStatement::orderBy('serial','asc')->get();
        $notice = Notice::all();
        return view('frontend.previous-police-super', compact('data', 'sliderImg', 'statement','mission_vision','statement_all','notice'));
    }
    public function PoliceOfficers()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $officer = PoliceOfficer::all();
        $notice = Notice::all();
        return view('frontend.officers', compact('data', 'sliderImg', 'statement','mission_vision','officer','notice'));
    }

    public function PoliceStaff()
    {
        $data=SiteSetting::first();
        $sliderImg=Slider::all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $staff = PoliceStaff::all();
        $notice = Notice::all();
        return view('frontend.police-staff', compact('data', 'sliderImg', 'statement','mission_vision','staff','notice'));
    }

    public function PoliceHelp()
    {
        $data=SiteSetting::first();
        $sliderImg=Slider::all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        return view('frontend.help', compact('data', 'sliderImg', 'statement','mission_vision','notice'));
    }
    public function UniteImage()
    {
        $data=SiteSetting::first();
        $sliderImg=Slider::all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        return view('frontend.unite-picture', compact('data', 'sliderImg', 'statement'));
    }
    public function CrimeComplain()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
       
        return view('frontend.crime-complain', compact('data', 'sliderImg'));
    }
    public function SmsInformation()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        
        return view('frontend.sms-information', compact('data', 'sliderImg'));
    }
    public function CriminalList()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $statement=PoliceStatement::orderBy('serial','desc')->first();
        $mission_vision=MissionVision::latest()->first();
        $notice = Notice::all();
        $criminal = CriminalList::all();
        return view('frontend.criminal-list', compact('data', 'sliderImg','statement','mission_vision','notice','criminal'));
    }
    public function PhotoGallery()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $gallery = PhotoGallery::all();
       
        
        return view('frontend.photo-gallery', compact('data', 'sliderImg','gallery'));
    }
     public function Download()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        $download = DownloadDocument::all();
        return view('frontend.download', compact('data', 'sliderImg','download'));
    }
    public function DistrictPolice()
    {
        $data=SiteSetting:: first();
        $sliderImg=Slider:: all();
        return view('frontend.district-police', compact('data', 'sliderImg'));
    }


}