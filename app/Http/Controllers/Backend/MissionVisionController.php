<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\MissionVision;

class MissionVisionController extends Controller
{
    public function showMissionVision()
    {
    	$mission_vision = MissionVision::all();
    	// dd($mission_vision);
    	return view('backend.mission_vision.mission-vision-show',compact('mission_vision'));
    }
    public function editMissionVision($id)
    {
    	$editData = MissionVision::find($id);
    	return view('backend.mission_vision.mission-vision-add',compact('editData'));
    }
    public function addMissionVision()
    {
        
        return view('backend.mission_vision.mission-vision-add');
    }
    public function storeMissionVision(Request $request)
    {
    	// dd($request->all());
        $request->validate([
            'mission'=> 'required',
            'vision'=> 'required',
        ]);
        // $mission_vision = new MissionVision;
        // $mission_vision->mission = $request->mission;
        // $mission_vision->vision = $request->vision;
        // $mission_vision->save();
        MissionVision::create($request->all());


        return redirect()->route('mission.vision.show');
    }
     public function updateMissionVision(Request $request,$id)
    {
    	// dd($request->all());
        $request->validate([
            'mission'=> 'required',
            'vision'=> 'required',
        ]);
    	$mission_vision = MissionVision::find($id);
       
        $mission_vision->update($request->all());


        return redirect()->route('mission.vision.show');

    }
    public function deleteMissionVision($id)
    {
        // dd($request->all());
        
        $mission_vision = MissionVision::find($id);    
        $mission_vision->delete();
        return redirect()->route('mission.vision.show');

    }
}
