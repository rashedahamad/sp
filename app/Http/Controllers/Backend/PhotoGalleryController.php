<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PhotoGallery;
use Image;

class PhotoGalleryController extends Controller
{
    //
    public function showPhotoGallery()
    {
    	$photo = PhotoGallery::all();
    	// dd($mission_vision);
    	return view('backend.photo_gallery.photo-gallery-view',compact('photo'));
    }
    public function editPhotoGallery($id)
    {
    	$editData = PhotoGallery::find($id);
    	return view('backend.photo_gallery.photo-gallery-add',compact('editData'));
    }
    public function addPhotoGallery()
    {
        
        return view('backend.photo_gallery.photo-gallery-add');
    }
    public function storePhotoGallery(Request $request)
    {
        $request->validate([
            'image' =>'required',
            
        ]);

        if ($file = $request->file('image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/photoGallery'), $filename);
            $image=Image::make(public_path('uploads/photoGallery/').$filename);
            $image->resize(480,600)->save(public_path('uploads/photoGallery/').$filename);
            $data = $request->all();
            $data['image']= $filename;
            PhotoGallery::create($data);
           
        }
        
        return redirect()->route('photo.gallery.show');
    }
     public function updatePhotoGallery(Request $request,$id)
    {
        $request->validate([
            
            'image' =>'required',
            
        ]);

        $data = $request->all();
        $photo = PhotoGallery::find($id);
    	
        if ($file = $request->file('image')) {
            @unlink(public_path('uploads/photoGallery/').$photo->image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/photoGallery'), $filename);
            $image=Image::make(public_path('uploads/photoGallery/').$filename);
            $image->resize(480,600)->save(public_path('uploads/photoGallery/').$filename);
            $data['image']= $filename;        
        }
        // dd($data);
        $photo->update($data);
        return redirect()->route('photo.gallery.show');

    }
    public function deletePhotoGallery($id)
    {
        $deleteData = PhotoGallery::find($id);
        // @unlink('public/uploads/officer/'.$deleteData->police_officer_image);
        $deleteData->delete();
        return redirect()->route('photo.gallery.show');
    }
}
