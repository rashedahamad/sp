<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\NecessaryLink;

class NecessaryLinkController extends Controller
{
    //
    public function showNecessaryLink()
    {
    	$link = NecessaryLink::all();
    	return view('backend.necessary_link.show-link',compact('link'));
    }
    public function addNecessaryLink()
    {

    	return view('backend.necessary_link.add-link');
    }
    public function storeNecessaryLink(Request $request)
    {

    	// dd($request->all());
        $request->validate([
            'link_name' => 'required',
            'link_address' => 'required',

        ]);
    	NecessaryLink::create($request->all());
        
        return redirect()->route('site.necessary.link');
    }
    public function editNecessaryLink($id)
    {
    	// dd($id);
    	$editData = NecessaryLink::find($id);
    	return view('backend.necessary_link.add-link',compact('editData'));
    }
     public function deleteNecessaryLink($id)
    {
    	$data = NecessaryLink::find($id);
    	$data->delete();
    	return redirect()->route('site.necessary.link');
    }
    public function updateNecessaryLink(Request $request,$id)
    {
       $request->validate([
            'link_name' => 'required',
            'link_address' => 'required',

        ]);
    	$update = NecessaryLink::find($id);
        $update->update($request->all());
        return redirect()->route('site.necessary.link');
    }
}
