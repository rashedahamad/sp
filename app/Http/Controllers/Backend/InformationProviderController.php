<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\InformationProvider;
use Image;
class InformationProviderController extends Controller
{
     public function showInformationProvider()
    {
    	$information_provider = InformationProvider::all();
    	// dd($mission_vision);
    	return view('backend.information_provider.information-provider-show',compact('information_provider'));
    }
    public function editInformationProvider($id)
    {
    	$editData = InformationProvider::find($id);
    	return view('backend.information_provider.information-provider-add',compact('editData'));
    }
    public function addInformationProvider()
    {
        
        return view('backend.information_provider.information-provider-add');
    }
    public function storeInformationProvider(Request $request)
    {
    	// dd($request->all());
        $request->validate([
            'information_provider_name' => 'required',
            'information_provider_designation' => 'required',
            'information_provider_address' => 'required',
            'information_provider_image' => 'required',

        ]);
        

        if ($file = $request->file('information_provider_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/informationProvider'), $filename);
            $image=Image::make(public_path('uploads/informationProvider/').$filename);
            $image->resize(480,600)->save(public_path('uploads/informationProvider/').$filename);
            $data = $request->all();
            $data['information_provider_image']= $filename;
            InformationProvider::create($data);
           
        }
        return redirect()->route('information.provider.show');
    }
     public function updateInformationProvider(Request $request,$id)
    {
    	$request->validate([
            'information_provider_name' => 'required',
            'information_provider_designation' => 'required',
            'information_provider_address' => 'required',

        ]);
            $data = $request->all();
            $provider = InformationProvider::find($id);
        if ($file = $request->file('information_provider_image')) {
            @unlink(public_path('uploads/informationProvider/').$provider->information_provider_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/informationProvider'), $filename);
            $image=Image::make(public_path('uploads/informationProvider/').$filename);
            $image->resize(480,600)->save(public_path('uploads/informationProvider/').$filename);
            
            $data['information_provider_image']= $filename;
        }
    	
        $provider->update($data);

        return redirect()->route('information.provider.show');

    }
    public function deleteInformationProvider($id)
    {
        $deleteData = InformationProvider::find($id);
        $deleteData->delete();
        return redirect()->route('information.provider.show');
    }
}
