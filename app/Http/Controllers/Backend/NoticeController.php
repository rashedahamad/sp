<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notice;
use Image;

class NoticeController extends Controller
{
    //
    public function showNotice()
    {
    	$notice = Notice::all();
    	return view('backend.notice.show-notice',compact('notice'));
    }
    public function addNotice()
    {

    	return view('backend.notice.add-notice');
    }
    public function storeNotice(Request $request)
    {
        $request->validate([
            'notice_title' => 'required',
            'notice_description' => 'required',
            'notice_date' => 'required',
            'notice_image' => 'required',

        ]);
        if ($file = $request->file('notice_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/notice'), $filename);
            $image=Image::make(public_path('uploads/notice/').$filename);
            $image->resize(600,600)->save(public_path('uploads/notice/').$filename);
            $data = $request->all();
            $data['notice_image']= $filename;
            Notice::create($data);
           
        }

    	
        return redirect()->route('site.notice');
    }
    public function editNotice($id)
    {
    	$editData = Notice::find($id);
    	return view('backend.notice.add-notice',compact('editData'));
    }
     public function deleteNotice($id)
    {
    	$data = Notice::find($id);
    	$data->delete();
    	return redirect()->route('site.notice');
    }
    public function updateNotice(Request $request,$id)
    {
    	// dd($request->all());
        $data = $request->all();
        $notice = Notice::find($id);
        
        if ($file = $request->file('notice_image')) {
            @unlink(public_path('uploads/notice/').$notice->notice_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/notice'), $filename);
            $image=Image::make(public_path('uploads/notice/').$filename);
            $image->resize(600,600)->save(public_path('uploads/notice/').$filename);
            $data['notice_image']= $filename;        
        }
    	$notice->update($data);
        return redirect()->route('site.notice');



    }
}
