<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CriminalList;
use Image;


class CriminalListController extends Controller
{
    public function showCriminalList()
    {
    	$criminal = CriminalList::all();
    	return view('backend.criminal_list.criminal-list-view',compact('criminal'));
    }
    public function addCriminalList()
    {

    	return view('backend.criminal_list.criminal-list-add');
    }
    public function storeCriminalList(Request $request)
    {
        $request->validate([
            'criminal_name' => 'required',
            'criminal_father' => 'required',
            'criminal_address' => 'required',
            'criminal_description' => 'required',
            'criminal_image' => 'required',

        ]);
        // dd($request->all());
        if ($file = $request->file('criminal_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/criminal'), $filename);
            $image=Image::make(public_path('uploads/criminal/').$filename);
            $image->resize(360,400)->save(public_path('uploads/criminal/').$filename);
            $data = $request->all();
            $data['criminal_image']= $filename;
            CriminalList::create($data);
           
        }
        return redirect()->route('criminal.list.show');
    }
    public function editCriminalList($id)
    {
    	$editData = CriminalList::find($id);
    	return view('backend.criminal_list.criminal-list-add',compact('editData'));
    }
     public function deleteCriminalList($id)
    {
    	$data = CriminalList::find($id);
    	$data->delete();
    	return redirect()->route('criminal.list.show');
    }
    public function updateCriminalList(Request $request,$id)
    {
    	// dd($request->all());
    	$request->validate([
            'criminal_name' => 'required',
            'criminal_father' => 'required',
            'criminal_address' => 'required',
            

        ]);
        $data = $request->all();
        $criminal = CriminalList::find($id);
        
        if ($file = $request->file('criminal_image')) {
            @unlink(public_path('uploads/criminal/').$criminal->criminal_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/criminal'), $filename);
            $image=Image::make(public_path('uploads/criminal/').$filename);
            $image->resize(600,600)->save(public_path('uploads/criminal/').$filename);
            $data['criminal_image']= $filename;        
        }
    	$criminal->update($data);
        return redirect()->route('criminal.list.show');
    }
}
