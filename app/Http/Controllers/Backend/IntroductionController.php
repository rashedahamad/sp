<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Introduction;
use App\Model\SiteSetting;

class IntroductionController extends Controller
{
    //
    public function viewIntro()
    {
    	$data = Introduction::first();
    	return view('backend.introduction.home-introduction-view',compact('data'));
    	
    }
    public function addIntro()
    {

    	return view('backend.introduction.home-introduction-add');
    	
    }
    public function updateIntro(Request $request,$id)
    {
        $request->validate([
            'introduction_title' => 'required',
            'introduction_description' => 'required',

        ]);
    	$introduction = Introduction::find($id);
        $data = $request->all();
    	$introduction->update($data); 


    	
    	return redirect()->route('view.introduction');
    	
    }
    public function editIntro($id)
    {
    	$editData = Introduction::find($id);

    	return view('backend.introduction.home-introduction-add',compact('editData'));
    	
    	
    	
    }
}
