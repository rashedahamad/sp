<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PoliceStaff;
use Image;

class PoliceStaffController extends Controller
{
    public function showPoliceStaff()
    {
    	$police_staff = PoliceStaff::all();
    	// dd($mission_vision);
    	return view('backend.staffs.police-staffs-show',compact('police_staff'));
    }
    public function editPoliceStaff($id)
    {
    	$editData = PoliceStaff::find($id);
    	return view('backend.staffs.police-staffs-add',compact('editData'));
    }
    public function addPoliceStaff()
    {
        
        return view('backend.staffs.police-staffs-add');
    }
    public function storePoliceStaff(Request $request)
    {
    	// dd($request->all());
         $request->validate([
            'police_staff_name' => 'required',
            'police_staff_designation' =>'required',
            'police_staff_devission' => 'required',
            'police_staff_image' => 'required',
            

        ]);
        

        if ($file = $request->file('police_staff_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/staff'), $filename);
            $image=Image::make(public_path('uploads/staff/').$filename);
            $image->resize(480,600)->save(public_path('uploads/staff/').$filename);
            $data = $request->all();
            $data['police_staff_image']= $filename;
            PoliceStaff::create($data);
           
        }
        


        return redirect()->route('police.staff.show');
    }
     public function updatePoliceStaff(Request $request,$id)
    {
        $request->validate([
            'police_staff_name' => 'required',
            'police_staff_designation' =>'required',
            'police_staff_devission' => 'required',
           
        ]);
    	
        $data = $request->all();
        $officer = PoliceStaff::find($id);
        
        if ($file = $request->file('police_staff_image')) {
            @unlink(public_path('uploads/staff/').$officer->police_staff_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/staff'), $filename);
            $image=Image::make(public_path('uploads/staff/').$filename);
            $image->resize(480,600)->save(public_path('uploads/staff/').$filename);
            $data['police_staff_image']= $filename;        
        }
        // dd($data);
        $officer->update($data);


    	


        return redirect()->route('police.staff.show');

    }
    public function deletePoliceStaff($id)
    {
        $deleteData = PoliceStaff::find($id);
        $deleteData->delete();
        return redirect()->route('police.staff.show');
    }
}
