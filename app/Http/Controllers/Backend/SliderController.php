<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Model\Slider;
use Image;


use ImageHelper;

class SliderController extends Controller
{
    public function index()
    {
    	// $data['slider'] = Slider::orderBy('updated_at','desc')->get();
          $data = Slider::all();
    	return view('backend.slider.view-slider',compact('data'));
    }

    public function addSlider()
    {
      

    	return view('backend.slider.add-slider-img');
    }

    public function storeSlider(Request $request)
    {
        $request->validate([
            'image' => 'required',
            

        ]);
        

        if ($file = $request->file('image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/slider'), $filename);
            $image=Image::make(public_path('uploads/slider/').$filename);
            $image->resize(2400,1200)->save(public_path('uploads/slider/').$filename);
            $data = $request->all();
            $data['image']= $filename;
            Slider::create($data);
           
        }

        return redirect()->route('site.slider');

    }

    public function deleteSlider($id)
    {
        
        $data = Slider::find($id);
        $data->delete();
        return redirect()->route('site.slider');
    }
}

