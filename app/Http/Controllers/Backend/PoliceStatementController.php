<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PoliceStatement;
use Image;

class PoliceStatementController extends Controller
{
    //
    public function showPoliceStatement()
    {
    	$data = PoliceStatement::orderBy('serial','asc')->get();
    	
    	return view('backend.police_statement.police-statement-show',compact('data'));
    }
    public function editPoliceStatement($id)
    {
    	$editData = PoliceStatement::find($id);
    	return view('backend.police_statement.police-statement-add',compact('editData'));
    }
    public function addPoliceStatement()
    {
        
        return view('backend.police_statement.police-statement-add');
    }
    public function storePoliceStatement(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'police_name' => 'required',
            'police_designation' => 'required',
            'police_joining_date' => 'required',
            'police_end_date' => 'required',
            'police_statement_title' => 'required',
            'police_short_description' => 'required',
            'police_long_description' => 'required',
            'police_image' => 'required',
            'serial' => 'required',

        ]);
        
        
        if ($file = $request->file('police_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/policeStatement'), $filename);
            $image=Image::make(public_path('uploads/policeStatement/').$filename);
            $image->resize(480,600)->save(public_path('uploads/policeStatement/').$filename);
            $data = $request->all();
            $data['police_image']= $filename;
            PoliceStatement::create($data);
           
        }
        
        return redirect()->route('site.police.statement.show');
    }
     public function updatePoliceStatement(Request $request,$id)
    {
    	// dd($request->all());
        $request->validate([
            'police_name' => 'required',
            'police_designation' => 'required',
            'police_joining_date' => 'required',
            'police_end_date' => 'required',
            'police_statement_title' => 'required',
            'police_short_description' => 'required',
            'police_long_description' => 'required',
            'serial' => 'required',
            

        ]);
        $data = PoliceStatement::find($id);
        $statement = $request->all();
        if ($file = $request->file('police_image')) {
            @unlink(public_path('uploads/policeStatement/').$data->police_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/policeStatement'), $filename);
            $image=Image::make(public_path('uploads/policeStatement/').$filename);
            $image->resize(480,600)->save(public_path('uploads/policeStatement/').$filename);
            $statement['police_image']= $filename;
            
           
        }
        $data->update($statement);
    	
        return redirect()->route('site.police.statement.show');

    }
}
