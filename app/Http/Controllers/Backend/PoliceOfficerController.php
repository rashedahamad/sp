<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PoliceOfficer;
use Image;

class PoliceOfficerController extends Controller
{
    public function showPoliceOfficer()
    {
    	$police_officer = PoliceOfficer::all();
    	// dd($mission_vision);
    	return view('backend.officers.police-officers-show',compact('police_officer'));
    }
    public function editPoliceOfficer($id)
    {
    	$editData = PoliceOfficer::find($id);
    	return view('backend.officers.police-officers-add',compact('editData'));
    }
    public function addPoliceOfficer()
    {
        
        return view('backend.officers.police-officers-add');
    }
    public function storePoliceOfficer(Request $request)
    {
        $request->validate([
            'police_officer_name' => 'required',
            'police_officer_designation' =>'required',
            'police_officer_devision' => 'required',
            'police_officer_image' => 'required',

        ]);
    	

        if ($file = $request->file('police_officer_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/officer'), $filename);
            $image=Image::make(public_path('uploads/officer/').$filename);
            $image->resize(480,600)->save(public_path('uploads/officer/').$filename);
            $data = $request->all();
            $data['police_officer_image']= $filename;
            PoliceOfficer::create($data);
           
        }
        
        return redirect()->route('police.officer.show');
    }
     public function updatePoliceOfficer(Request $request,$id)
    {
        $request->validate([
            'police_officer_name' => 'required',
            'police_officer_designation' =>'required',
            'police_officer_devision' => 'required',
        ]);

        $data = $request->all();
        $officer = PoliceOfficer::find($id);
    	
        if ($file = $request->file('police_officer_image')) {
            @unlink(public_path('uploads/officer/').$officer->police_officer_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/officer'), $filename);
            $image=Image::make(public_path('uploads/officer/').$filename);
            $image->resize(480,600)->save(public_path('uploads/officer/').$filename);
            $data['police_officer_image']= $filename;        
        }
        // dd($data);
        $officer->update($data);
        return redirect()->route('police.officer.show');

    }
    public function deletePoliceOfficer($id)
    {
        $deleteData = PoliceOfficer::find($id);
        // @unlink('public/uploads/officer/'.$deleteData->police_officer_image);
        $deleteData->delete();
        return redirect()->route('police.officer.show');
    }
}
