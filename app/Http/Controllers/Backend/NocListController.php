<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\NocList;
use Image;

class NocListController extends Controller
{
    //
    public function showNoc()
    {
    	$noc = NocList::all();
    	return view('backend.noc.show-noc',compact('noc'));
    }
    public function addNoc()
    {
        // dd('test');
    	return view('backend.noc.add-noc');
    }
    public function storeNoc(Request $request)
    {

    	
        $request->validate([
            'noc_name' =>'required',
            'noc_father_name' =>'required',
            'noc_address' =>'required',
            'noc_image' =>'required',

        ]);
    	if ($file = $request->file('noc_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/noc'), $filename);
            $image=Image::make(public_path('uploads/noc/').$filename);
            $image->resize(480,600)->save(public_path('uploads/noc/').$filename);
            $data = $request->all();
            $data['noc_image']= $filename;
            NocList::create($data);
           
        }
        return redirect()->route('site.noc');
    }
    public function editNoc($id)
    {
    	// dd($id);
    	$editData = NocList::find($id);
    	return view('backend.noc.add-noc',compact('editData'));
    }
     public function deleteNoc($id)
    {
    	$data = NocList::find($id);
    	$data->delete();
    	return redirect()->route('site.noc');
    }
    public function updateNoc(Request $request,$id)
    {
    	 $request->validate([
            'noc_name' =>'required',
            'noc_father_name' =>'required',
            'noc_address' =>'required',
        ]);
         $noc = NocList::find($id);
         $data = $request->all();
        if ($file = $request->file('noc_image')) {
             @unlink(public_path('uploads/noc/').$noc->noc_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/noc'), $filename);
            $image=Image::make(public_path('uploads/noc/').$filename);
            $image->resize(480,600)->save(public_path('uploads/noc/').$filename);
            $data['noc_image']= $filename; 
        }
        $noc->update($data);
        return redirect()->route('site.noc');



    }
}
