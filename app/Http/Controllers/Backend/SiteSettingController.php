<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SiteSetting;
use Image;
class SiteSettingController extends Controller
{
    public function index()
    {   	
        $data = SiteSetting::first();
        // dd($data->toArray());
    	return view('backend.settings.edit-site-settings',compact('data'));
    }

    // public function siteSettingsBn(){
    //     return view('backend.settings.edit-site-settings-bn');
    // } 
 
    public function storeLogo(Request $request, $id)
    {

        $request->validate([
            'site_logo' => 'required',
            'govt_logo' =>'required',
            

        ]);

        $store = SiteSetting::find($id);
        

        if ( $site = $request->file('site_logo')) {
            @unlink(public_path('uploads/logo/site_logo/').$store->site_logo);
            $sitename = date('Ymd') .'_'.time() . '.' . $site->getClientOriginalExtension();
            
            $site->move(public_path('uploads/logo/site_logo/'), $sitename);
            $image=Image::make(public_path('uploads/logo/site_logo/').$sitename);
            $image->resize(480,600)->save(public_path('uploads/logo/site_logo/').$sitename);


            if($govt = $request->file('govt_logo'))
            {
                @unlink(public_path('uploads/logo/govt_logo/').$store->govt_logo);
                $govtname = date('Ymd') .'_'.time() . '.' . $govt->getClientOriginalExtension();
                $govt->move(public_path('uploads/logo/govt_logo/'), $govtname);
                $image=Image::make(public_path('uploads/logo/govt_logo/').$govtname);
                $image->resize(480,600)->save(public_path('uploads/logo/govt_logo/').$govtname);
                $data = $request->all();
                $data['site_logo']= $sitename;
                $data['govt_logo']= $govtname;
            }

                
                $store->update($data);
            
           
           
        }
        
       // $govt = rand(111,222).'.'.request()->govt_logo->getClientOriginalExtension();
       //  request()->govt_logo->move(public_path('images'), $govt);
       //  $site = rand(333,444).'.'.request()->site_logo->getClientOriginalExtension();
       //  request()->site_logo->move(public_path('images'), $site);
       //  $img = new SiteSetting;
       //  // dd($site);
       //  $img->govt_logo = $govt;
       //  $img->site_logo = $site;
       //  $img->save(); 
        return redirect()->route('site.setting');


        
}
        

    // public function viewSlider()
    // {

    // }

    // public function storeSlider(Request $request)
    // {

    // }

    // public function editSlider($id)
    // {

    // }

    // public function updateSlider(Request $request)
    // {

    // }

    // public function deleteSlider($id)
    // {

    // }


}
