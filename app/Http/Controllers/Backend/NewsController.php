<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SiteSetting;
use App\Model\Slider;
use App\Model\News;
use Image;

class NewsController extends Controller
{
    //
	public function viewNews()
	{
		$data = News::all();
		return view('backend.news.view-news',compact('data'));

	}
	public function addNews()
	{
		return view('backend.news.add-news');

	}
	public function storeNews(Request $request)
	{
		$request->validate([
			'news_title' => 'required',
			'news_short' => 'required',
			'news_large' => 'required',
			'news_date' => 'required',
			'news_image' => 'required',

		]);
		 if ($file = $request->file('news_image')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/news'), $filename);
            $image=Image::make(public_path('uploads/news/').$filename);
            $image->resize(480,600)->save(public_path('uploads/news/').$filename);
            $data = $request->all();
            $data['news_image']= $filename;
            News::create($data);
           
        }
		
        return redirect()->route('view.site.news');
	}
	public function editNews($id)
	{
		$editData = News::find($id);
		return view('backend.news.add-news',compact('editData'));

	}
	public function updateNews(Request $request,$id)
	{
		$request->validate([
			'news_title' => 'required',
			'news_short' => 'required',
			'news_large' => 'required',
			'news_date' => 'required',
			

		]);
		$news = News::find($id);
		$data = $request->all();
		if ($file = $request->file('news_image')) {
			@unlink(public_path('uploads/news/').$news->news_image);
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/news'), $filename);
            $image=Image::make(public_path('uploads/news/').$filename);
            $image->resize(480,600)->save(public_path('uploads/news/').$filename);
            
            $data['news_image']= $filename;
            
           
        }
        $news->update($data);
		return redirect()->route('view.site.news');

	}
	public function deleteNews($id)
	{
		$news = News::find($id);
		$news->delete();
		return redirect()->route('view.site.news');
	}

    
}
