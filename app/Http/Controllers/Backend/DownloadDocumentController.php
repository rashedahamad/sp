<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\DownloadDocument;

class DownloadDocumentController extends Controller
{
    //
    public function showDownloadDocument()
    {
    	$download = DownloadDocument::all();
    	return view('backend.download.download-document-view',compact('download'));
    }
    public function addDownloadDocument()
    {

    	return view('backend.download.download-document-add');
    }
    public function storeDownloadDocument(Request $request)
    {

    	// dd($request->all());
        $request->validate([
            'name' => 'required',
            'publish_date' => 'required',
            'file' => 'required',

        ]);
        if ($file = $request->file('file')) {
            $filename =date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/document'), $filename);
            // $image=Image::make(public_path('uploads/document/').$filename);
            // $image->resize(480,600)->save(public_path('uploads/document/').$filename);
            $data = $request->all();
            $data['file']= $filename;
            DownloadDocument::create($data);
           
        }
    	
        
        return redirect()->route('download.document.show');
    }
    public function editDownloadDocument($id)
    {
    	// dd($id);
    	$editData = DownloadDocument::find($id);
    	return view('backend.download.download-document-add',compact('editData'));
    }
     public function deleteNecessaryLink($id)
    {
    	$data = DownloadDocument::find($id);
    	$data->delete();
    	return redirect()->route('download.document.show');
    }
    public function updateDownloadDocument(Request $request,$id)
    {
        $request->validate([
            'name' => 'required',
            'publish_date' => 'required',
            

        ]);
        $data = $request->all();
        $update = DownloadDocument::find($id);
        
        if ($file = $request->file('file')) {
            @unlink(public_path('uploads/document/').$update->file);
            $filename = date('Ymd') .'_'.time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('uploads/document'), $filename);
            // $image=Image::make(public_path('uploads/document/').$filename);
            // $image->resize(480,600)->save(public_path('uploads/document/').$filename);
            $data['file']= $filename;        
        }
    	
        $update->update($data);
        return redirect()->route('download.document.show');
    }
}
