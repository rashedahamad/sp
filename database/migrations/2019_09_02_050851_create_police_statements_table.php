<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliceStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('police_statements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('police_name');
            $table->string('police_designation')->nullable();
            $table->string('police_statement_title',500)->nullable();
            $table->text('police_short_description')->nullable();
            $table->text('police_long_description')->nullable();
            $table->string('police_image')->nullable();
            $table->integer('serial')->nullable();
            $table->date('police_joining_date')->nullable();
            $table->date('police_end_date')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('police_statements');
    }
}
