<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('information_provider_name');
            $table->string('information_provider_designation')->nullable();
            $table->string('information_provider_address')->nullable();
            $table->string('information_provider_image')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_providers');
    }
}
