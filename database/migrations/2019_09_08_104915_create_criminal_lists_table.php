<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCriminalListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('criminal_lists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('criminal_name');
            $table->string('criminal_father');
            $table->string('criminal_address');
            $table->text('criminal_description');
            $table->string('criminal_image');
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criminal_lists');
    }
}
