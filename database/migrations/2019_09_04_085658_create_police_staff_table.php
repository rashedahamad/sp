<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliceStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('police_staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('police_staff_name');
            $table->string('police_staff_designation')->nullable();
            $table->string('police_staff_devission')->nullable();
            $table->string('police_staff_image')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('police_staff');
    }
}
