<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliceOfficersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('police_officers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('police_officer_name');
            $table->string('police_officer_designation')->nullable();
            $table->string('police_officer_devission')->nullable();
            $table->string('police_officer_image')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('police_officers');
    }
}
