-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 29, 2019 at 04:49 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `introductions`
--

DROP TABLE IF EXISTS `introductions`;
CREATE TABLE IF NOT EXISTS `introductions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `introduction_title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction_description` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `introductions`
--

INSERT INTO `introductions` (`id`, `introduction_title`, `introduction_description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'লক্ষ্মীপুর জেলা পুলিশের পক্ষ থেকে আপনাকে স্বাগতম test', 'লক্ষীপুর পুলিশ সুপারের কার্যালয়, কোট বিল্ডিং এলাকায় একটি মনোরম পরিবেশে অবস্থিত। অত্র অফিস ১৯৮৭ ইং সালে কার্যক্রম শুরু করে। সমগ্র জেলা পুলিশ ইউনিটের সকল দাপ্তরিক কার্যক্রম অত্র কার্যালয় হতে নিয়ন্ত্রিত হয়ে থাকে। অত্র দপ্তর হতে লক্ষীপুরজেলার সার্বিক আইনশৃংখলা কার্যক্রম মনিটরিং করা হয়। জনসাধারনের জানমালের নিরাপত্তা ও জেলার আইন-শৃংখলা বিষয়ক নির্দেশ সমূহ অত্র কার্যালয়ের প্রধান মাননীয় পুলিশ সুপার দিয়ে থাকেন। update', NULL, '2019-08-24 23:55:40', '2019-08-25 00:56:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_08_01_090751_create_site_settings_table', 2),
(4, '2019_08_05_043423_create_sliders_table', 1),
(8, '2019_08_22_111935_create_news_table', 3),
(15, '2019_08_24_051344_create_news_table', 4),
(17, '2019_08_24_121314_create_indroductions_table', 5),
(18, '2019_08_25_054137_create_introductions_table', 6),
(24, '2019_08_25_082322_create_notices_table', 7),
(25, '2019_08_25_105015_create_noc_lists_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `news_title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_short` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_large` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_date` date DEFAULT NULL,
  `news_created_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_title`, `news_short`, `news_large`, `news_image`, `news_date`, `news_created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'আজ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন', 'আজ ২১ জুলাই ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন্ন হয়েছে। উক্ত সভায় উপস্থিত ছিলেন লক্ষ্মীপুর জেলার সম্মানীত পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা, অতিরিক্ত পুলিশ সুপার(সদর) জনাব পংকজ কুমার দে, সহকারী পুলিশ সুপার(রামগতি সার্কেল) জনাব মারুফা নাজনীন, সহকারী পুলিশ সুপার(রায়পুর সার্কেল) জনাব স্পিনা রানী প্রামাণিক, আর আই পুলিশ লাইন্স, আরও-১ সহ সকল থানার অফিসার ইনচার্জ, ফোর্স ও কর্মকর্তাবৃন্দ।', 'আজ ২১ জুলাই ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন্ন হয়েছে। উক্ত সভায় উপস্থিত ছিলেন লক্ষ্মীপুর জেলার সম্মানীত পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা, অতিরিক্ত পুলিশ সুপার(সদর) জনাব পংকজ কুমার দে, সহকারী পুলিশ সুপার(রামগতি সার্কেল) জনাব মারুফা নাজনীন, সহকারী পুলিশ সুপার(রায়পুর সার্কেল) জনাব স্পিনা রানী প্রামাণিক, আর আই পুলিশ লাইন্স, আরও-১ সহ সকল থানার অফিসার ইনচার্জ, ফোর্স ও কর্মকর্তাবৃন্দ। কল্যাণ সভায় জেলা পুলিশ ফোর্সের সুযোগ-সুবিধা সংক্রান্ত বিভিন্ন বিষয় নিয়ে আলোচনা করা হয় এবং অপরাধ সভায় জেলার আইন শৃঙ্খলা নিয়ন্ত্রণ, অবৈধ মাদকদ্রব্য, অস্ত্র উদ্ধার ও ওয়ারেন্টভুক্ত সাজাপ্রাপ্ত আসামী গ্রেপ্তার নিয়ে পর্যালোচনা করা হয় এবং সকল থানার অফিসারদের মধ্য থেকে বিভিন্ন পদবীর অফিসারবৃন্দকে উল্লেখযোগ্য সাফল্যের জন্য পুরস্কৃত করা হয়। এছাড়াও লক্ষ্মীপুর জেলায় কর্তব্যরত অবস্থা থেকে অবসরে যাওয়া ০২ জন পুলিশ সদস্যকে জেলা পুলিশের পক্ষ থেকে বিদায় সংবর্ধনা জানানো হয়।', '18558.jpg', '2019-08-07', NULL, NULL, '2019-08-24 02:46:11', '2019-08-24 02:46:11'),
(2, 'আজ লক্ষ্মীপুর জেলা পুলিশের মাসিক মাস্টার প্যারেড অনুষ্ঠিত হয়।', 'অদ্য ২৩ জুন ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশ লাইন্স প্যারেড গ্রাউন্ডে মাসিক মাস্টার প্যারেডের অভিবাদন গ্রহন করেন এবং সকল যানবাহন পরিদর্শণ করেন লক্ষ্মীপুর জেলার সুযোগ্য পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা মহোদয়।', 'অদ্য ২৩ জুন ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশ লাইন্স প্যারেড গ্রাউন্ডে মাসিক মাস্টার প্যারেডের অভিবাদন গ্রহন করেন এবং সকল যানবাহন পরিদর্শণ করেন লক্ষ্মীপুর জেলার সুযোগ্য পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা মহোদয়।', '45627.jpg', '2019-02-05', NULL, NULL, '2019-08-24 02:51:57', '2019-08-24 02:51:57');

-- --------------------------------------------------------

--
-- Table structure for table `noc_lists`
--

DROP TABLE IF EXISTS `noc_lists`;
CREATE TABLE IF NOT EXISTS `noc_lists` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `noc_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noc_father_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noc_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noc_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noc_lists`
--

INSERT INTO `noc_lists` (`id`, `noc_name`, `noc_father_name`, `noc_address`, `noc_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Akash', 'Karim', 'Fatulla', '1026289.jpg', NULL, '2019-08-25 06:25:17', '2019-08-25 06:25:17'),
(2, 'Safa', 'Ramjan', 'Narayanganj', '1256910.jpg', NULL, '2019-08-25 06:26:21', '2019-08-25 06:26:21');

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

DROP TABLE IF EXISTS `notices`;
CREATE TABLE IF NOT EXISTS `notices` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_description` varchar(6000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_posted_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `notice_title`, `notice_description`, `notice_date`, `notice_image`, `notice_posted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি', 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি\r\n০৮ ফেব্রুয়ারি ২০১৮:\r\nজননিরাপত্তা বিধান ও সেবার মহান ব্রতে নিজেকে গড়ে তুলতে বাংলাদেশ পুলিশ বাহিনীতে ট্রেইনি রিক্রুট কনস্টবল (টিআরসি) পদে নিয়োগ বিজ্ঞপ্তি প্রকাশ করা হয়েছে।\r\n\r\nএই বিজ্ঞপ্তিতে ৮৫০০ জন পুরুষ ও ১৫০০ জন নারী সর্বমোট ১০,০০০ জনকে রিক্রুট কনস্টবল (টিআরসি) পদে নিয়োগ দেওয়া হবে।\r\n\r\nআগ্রহী প্রার্থীদেরকে নিজ জেলার স্থায়ী বাসিন্দা হতে হবে। তাদেরকে শারীরিক মাপ, লিখিত পরীক্ষা, মৌখিক পরীক্ষা ও শারীরিক পরীক্ষায় অংশ গ্রহণ করতে হবে।', '2019-05-31', '80918.jpg', NULL, NULL, '2019-08-25 04:06:30', '2019-08-25 04:22:03'),
(3, 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি', 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি\r\n০৮ ফেব্রুয়ারি ২০১৮:\r\nজননিরাপত্তা বিধান ও সেবার মহান ব্রতে নিজেকে গড়ে তুলতে বাংলাদেশ পুলিশ বাহিনীতে ট্রেইনি রিক্রুট কনস্টবল (টিআরসি) পদে নিয়োগ বিজ্ঞপ্তি প্রকাশ করা হয়েছে।\r\n\r\nএই বিজ্ঞপ্তিতে ৮৫০০ জন পুরুষ ও ১৫০০ জন নারী সর্বমোট ১০,০০০ জনকে রিক্রুট কনস্টবল (টিআরসি) পদে নিয়োগ দেওয়া হবে।\r\n\r\nআগ্রহী প্রার্থীদেরকে নিজ জেলার স্থায়ী বাসিন্দা হতে হবে। তাদেরকে শারীরিক মাপ, লিখিত পরীক্ষা, মৌখিক পরীক্ষা ও শারীরিক পরীক্ষায় অংশ গ্রহণ করতে হবে।', '2019-08-15', '457595.jpg', NULL, NULL, '2019-08-25 04:32:21', '2019-08-25 04:32:21'),
(4, 'দুষ্টের দমন শিষ্টের পালন এই মহান ব্রত নিয়ে পুলিশ বাহিনীতে যোগ দিন', 'বাংলাদেশ পুলিশ বাহিনীতে যোগদিন\r\nবাংলাদেশ পুলিশের কনস্টেবল (পুরুষ ও নারী) নিয়োগ সার্কুলার হলো আজ।', '2019-08-15', '523622.jpg', NULL, NULL, '2019-08-25 04:33:17', '2019-08-25 04:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

DROP TABLE IF EXISTS `site_settings`;
CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_title` text COLLATE utf8mb4_unicode_ci,
  `site_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_title`, `site_logo`, `govt_logo`, `created_at`, `updated_at`) VALUES
(14, NULL, '348.jpg', '150.jpg', '2019-08-23 22:56:50', '2019-08-23 22:56:50'),
(13, NULL, '335.jpg', '111.jpg', '2019-08-22 00:15:29', '2019-08-22 00:15:29'),
(12, NULL, '387.jpg', '182.jpg', '2019-08-22 00:10:28', '2019-08-22 00:10:28'),
(11, NULL, '186.jpg', '1566454125.jpg', '2019-08-22 00:08:45', '2019-08-22 00:08:45');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `short_description`, `image`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '9979.jpg', '2019-08-22 00:42:46', '2019-08-22 00:42:46'),
(2, NULL, NULL, '7115.jpg', '2019-08-22 00:54:42', '2019-08-22 00:54:42'),
(3, NULL, NULL, '9177.jpg', '2019-08-23 22:56:02', '2019-08-23 22:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrtor', NULL, 'admin@gmail.com', NULL, '$2y$10$vuqF9VRgX/GZJxOth02HKeICIOFD1nFLW8pncA2ZblcLCWuZX3Mxe', NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
