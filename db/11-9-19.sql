-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 11, 2019 at 06:16 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `criminal_lists`
--

DROP TABLE IF EXISTS `criminal_lists`;
CREATE TABLE IF NOT EXISTS `criminal_lists` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `criminal_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criminal_father` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criminal_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `criminal_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `criminal_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `criminal_lists`
--

INSERT INTO `criminal_lists` (`id`, `criminal_name`, `criminal_father`, `criminal_address`, `criminal_description`, `criminal_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'Soyman Uddin Jishan', 'This is a Test.', 'sdgsgseg', 'Lakshmipur ps Case no-21(9)13, no-71(03)13,no-33(11)13, no-21(04)14, no- 02(03)14, no-93(04)14, no-0', '20190908_1567944873.jpg', NULL, '2019-09-08 06:14:33', '2019-09-08 06:14:33'),
(3, 'asad pong pong', 'pong png', 'kisorgonj', 'test', '20190909_1568030525.jpg', NULL, '2019-09-09 06:02:05', '2019-09-09 06:02:05');

-- --------------------------------------------------------

--
-- Table structure for table `download_documents`
--

DROP TABLE IF EXISTS `download_documents`;
CREATE TABLE IF NOT EXISTS `download_documents` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publish_date` date NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `download_documents`
--

INSERT INTO `download_documents` (`id`, `name`, `publish_date`, `file`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'CV', '2019-09-11', '20190909_1568026535.pdf', NULL, '2019-09-09 04:48:14', '2019-09-09 04:55:35'),
(2, 'IFIC', '2019-09-11', '20190909_1568026562.pdf', NULL, '2019-09-09 04:56:02', '2019-09-09 04:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `information_providers`
--

DROP TABLE IF EXISTS `information_providers`;
CREATE TABLE IF NOT EXISTS `information_providers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `information_provider_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `information_provider_designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information_provider_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information_provider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `information_providers`
--

INSERT INTO `information_providers` (`id`, `information_provider_name`, `information_provider_designation`, `information_provider_address`, `information_provider_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(4, 'Karim ahamad', 'ASP', 'Fatulla nayanaganj', '20190908_1567924833.png', NULL, '2019-09-08 00:35:49', '2019-09-08 00:40:33'),
(3, 'মোহাম্মদ ফয়েজ উদ্দীন', 'সহকারী পুলিশ সুপার', 'লক্ষ্মীপুর', '20190908_1567924803.jpg', NULL, '2019-09-04 00:41:46', '2019-09-08 00:40:03'),
(5, 'মোহাম্মদ ফয়েজ উদ্দীন', 'সহকারী পুলিশ সুপার', 'লক্ষ্মীপুর', '20190908_1567924864.jpg', NULL, '2019-09-08 00:41:04', '2019-09-08 00:41:04');

-- --------------------------------------------------------

--
-- Table structure for table `introductions`
--

DROP TABLE IF EXISTS `introductions`;
CREATE TABLE IF NOT EXISTS `introductions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `introduction_title` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction_description` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `introductions`
--

INSERT INTO `introductions` (`id`, `introduction_title`, `introduction_description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'লক্ষ্মীপুর জেলা পুলিশের পক্ষ থেকে আপনাকে স্বাগতম', 'লক্ষীপুর পুলিশ সুপারের কার্যালয়, কোট বিল্ডিং এলাকায় একটি মনোরম পরিবেশে অবস্থিত। অত্র অফিস ১৯৮৭ ইং সালে কার্যক্রম শুরু করে। সমগ্র জেলা পুলিশ ইউনিটের সকল দাপ্তরিক কার্যক্রম অত্র কার্যালয় হতে নিয়ন্ত্রিত হয়ে থাকে। অত্র দপ্তর হতে লক্ষীপুরজেলার সার্বিক আইনশৃংখলা কার্যক্রম মনিটরিং করা হয়। জনসাধারনের জানমালের নিরাপত্তা ও জেলার আইন-শৃংখলা বিষয়ক নির্দেশ সমূহ অত্র কার্যালয়ের প্রধান মাননীয় পুলিশ সুপার দিয়ে থাকেন।', NULL, '2019-08-24 23:55:40', '2019-09-07 10:42:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_08_01_090751_create_site_settings_table', 2),
(4, '2019_08_05_043423_create_sliders_table', 1),
(8, '2019_08_22_111935_create_news_table', 3),
(15, '2019_08_24_051344_create_news_table', 4),
(17, '2019_08_24_121314_create_indroductions_table', 5),
(18, '2019_08_25_054137_create_introductions_table', 6),
(24, '2019_08_25_082322_create_notices_table', 7),
(25, '2019_08_25_105015_create_noc_lists_table', 8),
(27, '2019_08_29_063419_create_necessary_links_table', 9),
(54, '2019_09_02_050851_create_police_statements_table', 17),
(39, '2019_09_03_111618_create_mission_visions_table', 11),
(40, '2019_09_04_055810_create_information_providers_table', 12),
(41, '2019_09_04_085553_create_police_officers_table', 13),
(42, '2019_09_04_085658_create_police_staff_table', 13),
(45, '2019_09_08_104915_create_criminal_lists_table', 14),
(47, '2019_09_09_064830_create_photo_galleries_table', 15),
(51, '2019_09_09_094542_create_download_documents_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `mission_visions`
--

DROP TABLE IF EXISTS `mission_visions`;
CREATE TABLE IF NOT EXISTS `mission_visions` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mission` text COLLATE utf8mb4_unicode_ci,
  `vision` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mission_visions`
--

INSERT INTO `mission_visions` (`id`, `mission`, `vision`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'বাংলাদেশকে একটি উন্নত ও সুরক্ষিত স্থান হিসাবে গড়ে তোলার লক্ষ্যে নাগরিকদের আস্থা ও শ্রদ্ধা ভোগযোগ্য, দক্ষ ও নিবেদিত পেশাদারদের দ্বারা মানসম্পন্ন পরিষেবা সরবরাহ করা।', 'To provide quality service by competent, efficient and dedicated professionals enjoying trust and respect of citizens to make Bangladesh a better and safer place to live.', NULL, '2019-09-03 05:53:57', '2019-09-07 22:36:47'),
(2, 'বাংলাদেশকে একটি উন্নত ও সুরক্ষিত স্থান হিসাবে গড়ে তোলার লক্ষ্যে নাগরিকদের আস্থা ও শ্রদ্ধা ভোগযোগ্য, দক্ষ ও নিবেদিত পেশাদারদের দ্বারা মানসম্পন্ন পরিষেবা সরবরাহ করা।', 'বাংলাদেশকে একটি উন্নত ও সুরক্ষিত স্থান হিসাবে গড়ে তোলার লক্ষ্যে নাগরিকদের আস্থা ও শ্রদ্ধা ভোগযোগ্য, দক্ষ ও নিবেদিত পেশাদারদের দ্বারা মানসম্পন্ন পরিষেবা সরবরাহ করা।', NULL, '2019-09-03 22:22:54', '2019-09-03 22:22:54');

-- --------------------------------------------------------

--
-- Table structure for table `necessary_links`
--

DROP TABLE IF EXISTS `necessary_links`;
CREATE TABLE IF NOT EXISTS `necessary_links` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `necessary_links`
--

INSERT INTO `necessary_links` (`id`, `link_name`, `link_address`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 'Cabinet Division', 'http://www.cabinet.gov.bd/', NULL, '2019-08-29 02:07:30', '2019-08-29 02:07:30'),
(2, 'জেলা প্রশাসক, লক্ষ্মীপুর', 'http://www.lakshmipur.gov.bd/', NULL, '2019-08-29 01:49:58', '2019-08-29 01:49:58'),
(4, 'লক্ষ্মীপুর, জেলা ওয়েব প্রোটাল', 'http://www.lakshmipur.gov.bd/', NULL, '2019-08-29 02:08:01', '2019-08-29 02:08:01'),
(5, 'Software Company', 'http://www.dynamicsoftbd.com/', NULL, '2019-08-29 02:44:30', '2019-08-29 02:44:30');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `news_title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_short` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_large` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `news_date` date DEFAULT NULL,
  `news_created_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `news_title`, `news_short`, `news_large`, `news_image`, `news_date`, `news_created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'আজ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন', 'আজ ২১ জুলাই ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন্ন হয়েছে। উক্ত সভায় উপস্থিত ছিলেন লক্ষ্মীপুর জেলার সম্মানীত পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা, অতিরিক্ত পুলিশ সুপার(সদর) জনাব পংকজ কুমার দে, সহকারী পুলিশ সুপার(রামগতি সার্কেল) জনাব মারুফা নাজনীন, সহকারী পুলিশ সুপার(রায়পুর সার্কেল) জনাব স্পিনা রানী প্রামাণিক, আর আই পুলিশ লাইন্স, আরও-১ সহ সকল থানার অফিসার ইনচার্জ, ফোর্স ও কর্মকর্তাবৃন্দ।', 'আজ ২১ জুলাই ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন্ন হয়েছে। উক্ত সভায় উপস্থিত ছিলেন লক্ষ্মীপুর জেলার সম্মানীত পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা, অতিরিক্ত পুলিশ সুপার(সদর) জনাব পংকজ কুমার দে, সহকারী পুলিশ সুপার(রামগতি সার্কেল) জনাব মারুফা নাজনীন, সহকারী পুলিশ সুপার(রায়পুর সার্কেল) জনাব স্পিনা রানী প্রামাণিক, আর আই পুলিশ লাইন্স, আরও-১ সহ সকল থানার অফিসার ইনচার্জ, ফোর্স ও কর্মকর্তাবৃন্দ। কল্যাণ সভায় জেলা পুলিশ ফোর্সের সুযোগ-সুবিধা সংক্রান্ত বিভিন্ন বিষয় নিয়ে আলোচনা করা হয় এবং অপরাধ সভায় জেলার আইন শৃঙ্খলা নিয়ন্ত্রণ, অবৈধ মাদকদ্রব্য, অস্ত্র উদ্ধার ও ওয়ারেন্টভুক্ত সাজাপ্রাপ্ত আসামী গ্রেপ্তার নিয়ে পর্যালোচনা করা হয় এবং সকল থানার অফিসারদের মধ্য থেকে বিভিন্ন পদবীর অফিসারবৃন্দকে উল্লেখযোগ্য সাফল্যের জন্য পুরস্কৃত করা হয়। এছাড়াও লক্ষ্মীপুর জেলায় কর্তব্যরত অবস্থা থেকে অবসরে যাওয়া ০২ জন পুলিশ সদস্যকে জেলা পুলিশের পক্ষ থেকে বিদায় সংবর্ধনা জানানো হয়।', '20190909_1568006278.jpg', '2019-08-07', NULL, NULL, '2019-08-24 02:46:11', '2019-09-08 23:17:59'),
(2, 'আজ লক্ষ্মীপুর জেলা পুলিশের মাসিক মাস্টার প্যারেড অনুষ্ঠিত হয়।', 'অদ্য ২৩ জুন ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশ লাইন্স প্যারেড গ্রাউন্ডে মাসিক মাস্টার প্যারেডের অভিবাদন গ্রহন করেন এবং সকল যানবাহন পরিদর্শণ করেন লক্ষ্মীপুর জেলার সুযোগ্য পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা মহোদয়।', 'অদ্য ২৩ জুন ২০১৯ খ্রিঃ তারিখ লক্ষ্মীপুর জেলা পুলিশ লাইন্স প্যারেড গ্রাউন্ডে মাসিক মাস্টার প্যারেডের অভিবাদন গ্রহন করেন এবং সকল যানবাহন পরিদর্শণ করেন লক্ষ্মীপুর জেলার সুযোগ্য পুলিশ সুপার জনাব আ স ম মাহাতাব উদ্দিন পিপিএম সেবা মহোদয়।', '20190909_1568006269.jpg', '2019-02-05', NULL, NULL, '2019-08-24 02:51:57', '2019-09-08 23:17:49'),
(4, 'আজ লক্ষ্মীপুর জেলা পুলিশের মাসিক কল্যাণ সভা ও আইন শৃঙ্খলা বিষয়ক আলোচনা সভা সম্পন', 'redwan', 'ahamad', '20190908_1567921582.png', '2019-09-11', NULL, NULL, '2019-09-07 23:38:08', '2019-09-07 23:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `noc_lists`
--

DROP TABLE IF EXISTS `noc_lists`;
CREATE TABLE IF NOT EXISTS `noc_lists` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `noc_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noc_father_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noc_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `noc_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `noc_lists`
--

INSERT INTO `noc_lists` (`id`, `noc_name`, `noc_father_name`, `noc_address`, `noc_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Akash', 'Karim', 'Fatulla', '20190908_1567935509.jpg', NULL, '2019-08-25 06:25:17', '2019-09-08 03:38:30'),
(4, 'Fea', 'Aziz', 'Saudi', '20190908_1567935518.jpg', NULL, '2019-09-08 02:18:04', '2019-09-08 03:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

DROP TABLE IF EXISTS `notices`;
CREATE TABLE IF NOT EXISTS `notices` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `notice_title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_description` varchar(6000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_date` date NOT NULL,
  `notice_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notice_posted_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `notice_title`, `notice_description`, `notice_date`, `notice_image`, `notice_posted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(5, 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি', 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি', '2019-09-01', '20190908_1567935761.jpg', NULL, NULL, '2019-09-08 00:50:54', '2019-09-08 03:42:42'),
(6, 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি', 'gxgseg', '2019-09-01', '20190908_1567935770.jpg', NULL, NULL, '2019-09-08 01:06:27', '2019-09-08 03:42:50'),
(3, 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি', 'বাংলাদেশ পুলিশে ১০ হাজার কনস্টবল নিয়োগ বিজ্ঞপ্তি\r\n০৮ ফেব্রুয়ারি ২০১৮:\r\nজননিরাপত্তা বিধান ও সেবার মহান ব্রতে নিজেকে গড়ে তুলতে বাংলাদেশ পুলিশ বাহিনীতে ট্রেইনি রিক্রুট কনস্টবল (টিআরসি) পদে নিয়োগ বিজ্ঞপ্তি প্রকাশ করা হয়েছে।\r\n\r\nএই বিজ্ঞপ্তিতে ৮৫০০ জন পুরুষ ও ১৫০০ জন নারী সর্বমোট ১০,০০০ জনকে রিক্রুট কনস্টবল (টিআরসি) পদে নিয়োগ দেওয়া হবে।\r\n\r\nআগ্রহী প্রার্থীদেরকে নিজ জেলার স্থায়ী বাসিন্দা হতে হবে। তাদেরকে শারীরিক মাপ, লিখিত পরীক্ষা, মৌখিক পরীক্ষা ও শারীরিক পরীক্ষায় অংশ গ্রহণ করতে হবে।', '2019-08-15', '20190908_1567935580.jpg', NULL, NULL, '2019-08-25 04:32:21', '2019-09-08 03:39:40');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photo_galleries`
--

DROP TABLE IF EXISTS `photo_galleries`;
CREATE TABLE IF NOT EXISTS `photo_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photo_galleries`
--

INSERT INTO `photo_galleries` (`id`, `title`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, NULL, '20190909_1568020201.jpg', NULL, '2019-09-09 03:09:49', '2019-09-09 03:10:01'),
(3, NULL, '20190909_1568020240.jpg', NULL, '2019-09-09 03:10:11', '2019-09-09 03:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `police_officers`
--

DROP TABLE IF EXISTS `police_officers`;
CREATE TABLE IF NOT EXISTS `police_officers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `police_officer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `police_officer_designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `police_officer_devision` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `police_officer_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `police_officers`
--

INSERT INTO `police_officers` (`id`, `police_officer_name`, `police_officer_designation`, `police_officer_devision`, `police_officer_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(29, 'আনোয়ার হোসেন', 'অতিরিক্ত পুলিশ সুপার', '(সদর সার্কেল,লক্ষ্মীপুর)', '20190905_1567665073.jpg', NULL, '2019-09-05 00:27:13', '2019-09-05 03:32:08'),
(31, 'সুমন রঞ্জন সরকার', 'অতিরিক্ত পুলিশ সুপার', '(সদর সার্কেল,লক্ষ্মীপুর)', '20190907_1567868618.jpg', '2019-09-09 11:59:12', '2019-09-07 09:03:38', '2019-09-09 05:59:12');

-- --------------------------------------------------------

--
-- Table structure for table `police_staff`
--

DROP TABLE IF EXISTS `police_staff`;
CREATE TABLE IF NOT EXISTS `police_staff` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `police_staff_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `police_staff_designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `police_staff_devission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `police_staff_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `police_staff`
--

INSERT INTO `police_staff` (`id`, `police_staff_name`, `police_staff_designation`, `police_staff_devission`, `police_staff_image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'মোঃ আল আমিন', 'কনস্টবল', 'আই সি টি শাখা', '20190907_1567868513.jpg', NULL, '2019-09-04 03:56:53', '2019-09-07 09:01:54'),
(2, 'মোঃ আজিজুল হক', 'এএসআই (সঃ)', 'হিসাব শাখা', '20190907_1567868528.jpg', NULL, '2019-09-04 04:01:45', '2019-09-07 09:02:08'),
(8, 'Redwan Ahamad', 'Programmer', 'ICT', '20190908_1567916615.jpg', NULL, '2019-09-07 09:02:42', '2019-09-07 22:23:35');

-- --------------------------------------------------------

--
-- Table structure for table `police_statements`
--

DROP TABLE IF EXISTS `police_statements`;
CREATE TABLE IF NOT EXISTS `police_statements` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `police_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `police_designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `police_statement_title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `police_short_description` text COLLATE utf8mb4_unicode_ci,
  `police_long_description` text COLLATE utf8mb4_unicode_ci,
  `police_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serial` int(11) DEFAULT NULL,
  `police_joining_date` date DEFAULT NULL,
  `police_end_date` date DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `police_statements`
--

INSERT INTO `police_statements` (`id`, `police_name`, `police_designation`, `police_statement_title`, `police_short_description`, `police_long_description`, `police_image`, `serial`, `police_joining_date`, `police_end_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'জনাব আ স ম মাহাতাব উদ্দিন', 'Police Super', 'পুলিশ সুপারের বক্তব্য', 'It\'s a test for short description.', 'It\'s a test for long description', '20190911_1568178493.jpg', 10, '2019-09-01', '2019-09-10', NULL, '2019-09-10 23:08:13', '2019-09-10 23:08:13'),
(2, 'Jahid', 'SP', 'পুলিশ সুপারের বক্তব্য', 'Its a tesst from short.', 'Its a tesst from Long.', '20190911_1568178799.jpg', 5, '2019-09-01', '2019-09-24', NULL, '2019-09-10 23:13:19', '2019-09-10 23:13:19'),
(3, 'Mehedi', 'SP', 'পুলিশ সুপারের বক্তব্য', 'Its a tesst from short.', 'Its a tesst from Long.', '20190911_1568178850.png', 15, '2019-09-01', '2019-09-30', NULL, '2019-09-10 23:14:10', '2019-09-10 23:14:10'),
(4, 'Redwan Ahamad', 'Police Super', 'পুলিশ সুপারের বক্তব্য', 'dgsrg', 'sgwsrgbsdrgsrg', '20190911_1568180922.jpg', 6, '2019-09-01', '2019-09-30', NULL, '2019-09-10 23:48:42', '2019-09-10 23:48:42'),
(5, 'জনাব আ স ম মাহাতাব উদ্দিন', 'SP', 'পুলিশ সুপারের বক্তব্য', 'eeeee', 'gsegsefs', '20190911_1568181099.jpg', 50, '2019-09-01', '2019-09-30', NULL, '2019-09-10 23:51:39', '2019-09-10 23:51:39'),
(6, 'Karim', 'SP', 'পুলিশ সুপারের বক্তব্য', 'ttttttttt', 'ttttttttttt', '20190911_1568181151.png', 2, '2019-09-01', '2019-09-30', NULL, '2019-09-10 23:52:31', '2019-09-10 23:52:31'),
(7, 'Sakib', 'SP', 'পুলিশ সুপারের বক্তব্য', 'rrrrrrr', 'rrereree', '20190911_1568181557.jpg', 51, '2019-09-11', '2019-09-30', NULL, '2019-09-10 23:59:18', '2019-09-10 23:59:18'),
(8, 'Jahid', 'পুলিশ   সুপারের   বক্তব্য', 'পুলিশ সুপারের বক্তব্য', 'eeeeeeeeeeee', 'fffffffffff', '20190911_1568181596.jpg', 30, '2019-09-02', '2019-09-22', NULL, '2019-09-10 23:59:57', '2019-09-10 23:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

DROP TABLE IF EXISTS `site_settings`;
CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_title` text COLLATE utf8mb4_unicode_ci,
  `site_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `site_title`, `site_logo`, `govt_logo`, `created_at`, `updated_at`) VALUES
(19, 'Logos', '20190908_1567917012.png', '20190908_1567917012.png', NULL, '2019-09-07 22:30:12');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `short_description`, `image`, `created_at`, `updated_at`) VALUES
(7, NULL, NULL, '20190909_1568010491.jpg', '2019-09-09 00:28:11', '2019-09-09 00:28:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrtor', NULL, 'admin@gmail.com', NULL, '$2y$10$vuqF9VRgX/GZJxOth02HKeICIOFD1nFLW8pncA2ZblcLCWuZX3Mxe', NULL, 'LYlmLOAHy3kQh7XXIcrrkeZDRbugl0FOvwR8XXkHEA2bgmV0AGKDb8hxhD5g', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
